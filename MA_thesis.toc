\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\contentsline {chapter}{Abstract}{i}{section*.1}%
\contentsline {chapter}{Acknowledgment}{iii}{chapter*.2}%
\contentsline {chapter}{List of Figures}{vii}{section*.5}%
\contentsline {chapter}{List of Tables}{ix}{chapter*.6}%
\contentsline {chapter}{Abbreviations}{xi}{chapter*.11}%
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Research problem}{2}{section.1.1}%
\contentsline {section}{\numberline {1.2}Methodologies}{3}{section.1.2}%
\contentsline {section}{\numberline {1.3}Document Outline}{4}{section.1.3}%
\contentsline {chapter}{\numberline {2}Background and related work}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Neuron model}{5}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Integrate and Fire models}{6}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Hodgkin-Huxley model}{8}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}Synapse models}{8}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Conductance-based synapse}{8}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Current-based synapse}{9}{subsection.2.2.2}%
\contentsline {section}{\numberline {2.3}Numerical solvers}{9}{section.2.3}%
\contentsline {section}{\numberline {2.4}Software pipelining}{10}{section.2.4}%
\contentsline {section}{\numberline {2.5}Related Projects}{11}{section.2.5}%
\contentsline {subsection}{\numberline {2.5.1}NEST simulator}{11}{subsection.2.5.1}%
\contentsline {subsection}{\numberline {2.5.2}Loihi 2}{12}{subsection.2.5.2}%
\contentsline {subsection}{\numberline {2.5.3}The SpiNNaker project}{12}{subsection.2.5.3}%
\contentsline {subsection}{\numberline {2.5.4}FPGA-based designs}{13}{subsection.2.5.4}%
\contentsline {section}{\numberline {2.6}Flynn's taxonomy}{13}{section.2.6}%
\contentsline {chapter}{\numberline {3}Design decisions and challenges}{15}{chapter.3}%
\contentsline {section}{\numberline {3.1}Processor type}{16}{section.3.1}%
\contentsline {section}{\numberline {3.2}Memory organization}{17}{section.3.2}%
\contentsline {section}{\numberline {3.3}Instruction Dependencies}{18}{section.3.3}%
\contentsline {section}{\numberline {3.4}Branches}{18}{section.3.4}%
\contentsline {section}{\numberline {3.5}Bypassing}{21}{section.3.5}%
\contentsline {chapter}{\numberline {4}Implementation details}{23}{chapter.4}%
\contentsline {section}{\numberline {4.1}CPU design I}{24}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Predicated execution}{27}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Instruction set and encoding}{28}{subsection.4.1.2}%
\contentsline {subsection}{\numberline {4.1.3}Limitations}{31}{subsection.4.1.3}%
\contentsline {section}{\numberline {4.2}CPU design II}{31}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Modifications to instruction}{31}{subsection.4.2.1}%
\contentsline {subsubsection}{\nonumberline Split pipeline approach}{32}{section*.36}%
\contentsline {subsubsection}{\nonumberline Memory operations}{36}{section*.37}%
\contentsline {subsubsection}{\nonumberline Increased variety of operands}{37}{section*.38}%
\contentsline {subsubsection}{\nonumberline Predication changes}{37}{section*.39}%
\contentsline {subsection}{\numberline {4.2.2}Instruction set}{40}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Assembly code}{40}{subsection.4.2.3}%
\contentsline {chapter}{\numberline {5}Results and measurements}{45}{chapter.5}%
\contentsline {section}{\numberline {5.1}Software pipelined code generation}{45}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Assembly code}{46}{subsection.5.1.1}%
\contentsline {section}{\numberline {5.2}RISC-V implementation}{48}{section.5.2}%
\contentsline {section}{\numberline {5.3}Performance and hardware utilization(Voltage loop)}{49}{section.5.3}%
\contentsline {chapter}{\numberline {6}Conclusion}{53}{chapter.6}%
\contentsline {section}{\numberline {6.1}Future improvements}{54}{section.6.1}%
\contentsline {chapter}{\nonumberline Bibliography}{55}{chapter*.45}%
