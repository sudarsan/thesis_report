---
title: "Guide: Scientific Writing"
author: Johnson Loh
date: 2020-12-14
keywords:
crossreference:
  - Guide_LiteratureResearch
bibliography:
csl:
---

# General

This document contains some guidelines for efficient scientific writing.

# Preparation

- Indentify research question
    - adressed problem
    - context of research domain
    - context of previous solutions
    - set of provided solutions (Own contribution)
- Extensive state-of-the-art research covering most important and newest relevant works
    - see [Guide: Literature Research](./Guide_LiteratureResearch.md)
- Collect and organize relevant references and data
    - archive result data / references (e.g. csv/bibtex)
    - archive scripts (e.g. \*.sh, \*.py, \*.m etc.)
    - standardized figures (generated from results: e.g. pyplot/matlab)

# General Workflow

In the following an example workflow is sketched to develop solutions for a target problem.

1. **Specification**
    - general high level description of the proposed solution
    - definition of function requirements
    - definition of non-functional requirements (quality of solution)
2. **Implementation**
    - select a design strategy for implementing the proposed solution (software-, hardware-or hybrid-based approach)
3. **Validation**
    - verification of the functional requirements defined at the *Specification* step
4. **Evaluation**
    - verification of the non-functional requirements defined at the *Specification* step
    - comparison between the proposed solution and the state-of-the-art based on previously selected metrics(performance, area overhead, etc.)

# Paper Structure

In the following an example structure is outlined for the written report.

0. **Abstract** 
1. **Introduction**
2. **Background**
3. **Proposed Approach**
4. **Experimental Setup**
5. **Results and Discussion**
6. **Conclusion**
7. **References**

The structure may vary based on research domain and correcponding established routines. Specific template styles for conferences and journals may cause a deviation from the pattern above.

The categories in the example structure does not necessarily correspond to individual paper sections. The length and detail of each section may vary based on research domain.

## Abstract

- Brief description of the proposed work including:
    - work’s context
    − addressed challenges/problems
    − very brief summary of proposed solution
    − key implementation details
    − key figures of obtained results
- Sometimes it is necessary to include index terms
    - search terms in databases
    - ~5-6 relevant keywords to describe proposed work

## Introduction

- Extension of abstract adressing
    - research context and motivation
    - addressed challenges/problems
    - previous works related to introduced challenge/problem
    - brief description of own proposed work, info about
        - implementation strategy
        - experimental setup adopted for validation and evaluation

- **Important note**:
    - Here, state-of-the-art covers a *variety of approaches able to address the introduced challenge/problem similar to the proposed solution*
    
## Background

- Summary of concepts/definitions crucial for complete understanding of proposed work
    - e.g. established norms and benchmarks, target architectures etc.
    - all concepts/definitions needs to be referenced
- Detailed outline of previous works
    - identification of validation strategies
    - identification of evaluation metrics
    - -> used to guide evaluation of own work
    
## Proposed Approach

- Detailed description of specification and implementation of proposed work
- Specification
    - high level description (block diagrams, flow charts, etc.)
    - definition of functional requirements (e.g. arithmetic operations etc.)
    - definition of non-functional requirements (qualitative metrics: e.g. area, energy etc.)
- Implementation
    - detailed description of adopted strategy to implement proposed approach
    
## Experimental Setup

- Experimental setup defined for validation
    - definition of operating/environment conditions for validation
    - description of used tools (e.g. EDA tools, NN frameworks)
    - check if functional requirements are met
- Normalized testbench setup
    - normalized and reproducable results
    - documented & repeatable set of stimuli to test average/corner cases
    - test design under test (DUT) or unit under test (UUT)
    
## Results and Discussion

- Evaluation of proposed approach
    - check if non-functional requirements are met
    - visualization of results using graphs and/or tables
    - interpretation of all presented results is necessary
- Comparison with state-of-the-art
    - use of evaluation metrics from *Background* section
    
## Conclusion

- Summary of presented results and their trends
    - Key contribution/figures
- Put results into context of addressed problem

- Future works (only promising outlooks)

## References

- List of all cited works
    - *no non-cited references*
- Common reference format
    - **IEEE**, MLA, APA etc.
    - abbreviation of authors list (3-4 authors + "et al.")
    - conference/journal name may be abbreviated
    - adhere to corresponding publishing entity
    
# Interesting reads

- Handbook for Technical Writers and Editors (NASA Edition) [^1]

[^1]: Mary K. McCaskill. Grammar, Punctuation, and Capitalization: A Handbook for Technical Writers and Editors, NASA SP-7084, available from https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19900017394.pdf

<!--
# References
-->