---
title: "Guide: Information on BA/MA Theses"
author: Johnson Loh
date: 2020-12-14
keywords:
crossreference:
  - Guide_LiteratureResearch
  - Templates/Template_IDSStatusUpdate
bibliography:
csl:
---

# General

This document contains some information about the procedure of a bachelor or master thesis.

# Recommended schedule and milestones

Milestones in the proposed schedule are marked by **Mx**. Regular status updates in steps 3-4 can be held to track progress (**M2**) ([Slide template](./Templates/Template_IDSStatusUpdate.pptx)).

1. Setup environment
    - [ ] Setup PC account with IT
    - [ ] Reserve a seat in the booking system
    - [ ] Get access from the supervisor for necessary repositories 
        - [ ] [Gitlab Wiki](https://git.rwth-aachen.de/ids/https://git.rwth-aachen.de/ids/wiki
        - [ ] [Template: Masterthesis Latex](https://git.rwth-aachen.de/ids/templates/masterthesis_latex_template)
        - [ ] Project specific
    - [ ] Get key from UMIC ITsec (preliminary email from supervisor, 2nd floor Mr Kerber)
    - [ ] Get access to communication platform (Teams or similar)
2. Plan time schedule
    - [ ] Create a roadmap for intended experiments/designs
    - [ ] Submit ZPA document
    - [ ] (Receive official start day email from secretary) (**M1**)
3. Literature review
    - [ ] see [Guide: Literature Research](./Guide_LiteratureResearch.md)
4. Experiments and implementation
    - [ ] IMPORTANT: systematic research methodology
5. Results evaluation and interpretation
    - [ ] IMPORTANT: state-of-the-art comparison
6. Write report
    - [ ] see [Guide: Scientific Writing](./Guide_ScientificWriting.md)
    - [ ] opportunity to hand-in preliminary version for correction to supervisor at latest 2 weeks before submission day (**M3**)
    - [ ] hand-in final version on submission day (**M4**)
        - [ ] due before midnight
        - [ ] email to immediate supervisor, info@ids.rwth-aachen.de, mikkin@ids.rwth-aachen.de, gemmeke@ids.rwth-aachen.de
        - no modifications are possible after submission day deadline
7. Prepare final presentation
    - [ ] select day for final presentation 
    - [ ] opportunity for trial presentation at latest 2 (working) days before final presentation day (**M5**)
    - [ ] final presentation (**M6**)
8. Finalization
    - [ ] summarize and structure research results (According to Template_Workfolder)
    - [ ] return key

# Assessment Criteria

1. Quality of work
    - Research method/design (e.g. explanation and justification of used principles etc.)
    - Application/development of theory (e.g. selection/development + integration of theory within the context of the project)
    - Interpretation of results (e.g. verification, summary and extrapolation of results etc.)
    - Scientific significance 
2. Performance
    - Critical attitude (e.g. towards own work, literature and specialists)
    - Creativity (e.g. original contribution to project)
    - Initiative
    - Interaction with peers/superiors
    - Planning (e.g. creation of project plan + execution)
3. Report
    - Content
    - Form (e.g. structure, visual aids etc.)
    - Quality of writing (e.g. spelling, grammatics, transitions etc.)
    - Independence in writing
4. Presentation and Defence
    - Content
    - Form (e.g. structure, visual aids etc.)
    - Performance (e.g. audience addressing, expression etc.)
    - Defence (argumentation in response to audience questions)

<!--
# References
-->