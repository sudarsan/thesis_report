---
title: "Guide: Literature Research"
author: Johnson Loh
date: 2020-12-14
keywords:
  - Database_LiteratureMain
  - Database_LiteratureOthers
crossreference:
bibliography:
csl:
---

# General

This document contains some guidelines for efficient literature review.
IMPORTANT: Use the 'references' folder to save all the literature used in your thesis (preferably as .pdf).

## Example procedure

1. Broad search
    - Tools:
        - Wikipedia (not suited as a direct reference, but good for initial idea)
        - Review Papers
        - Online Tools for Connectivity Graphs [^1]
    - Initial search should target the exploration of existing projects in terms of diversity, depth and relevance for your target research question
    - First skim over the content to get an idea of paper content (in-depth understanding is only necessary if it is relevant)
        - Some methods are discussed here [^2]
2. Targeted search
    - Tools:
        - Online databases (Google Scholar, Microsoft Academic, IEEE Xplore, ACM Digital Library, ResearchGate etc.)
        - Scientific Magazines & Journals
        - Books
    - Select outstanding papers from the broad search for detailed analysis
    - Given those landmark papers in the field, some further reading can be found in the references of those papers
    - Build an understanding of key techniques used
3. Analysis of state-of-the-art (SotA)
    - Structuring of knowledge:
        - Categorization of works by techniques/implementations
        - Categorization of works by relevance to own research question
    - Summary of knowledge:
        - Indentify technical expressions
        - Extract claimed results (quantitative values)

[^1]: Connected papers - visual tool to find and explore papers, [Online], Accessed: 14.12.2020, https://www.connectedpapers.com/
[^2]: ScienceMag Article - How to (seriously) read a scientific paper, [Online], Accessed: 14.12.2020, https://www.sciencemag.org/careers/2016/03/how-seriously-read-scientific-paper

<!--
# References
-->
