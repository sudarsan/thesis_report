%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%																
%	Chapter 1 - Introduction and Motivation 															 
%																				 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Introduction}

The idea of machine intelligence draws inspiration from the functioning of the brain. The necessity to replicate biological neural networks resulted in discoveries in the field of \acp{ANN}. Increasing availability of large amounts of data and computing power allowed for the development of huge \ac{ANN} models which could handle complex tasks. Models like switch transformers\cite{fedus2022switch} are capable of handling more than 1 trillion parameters and are used in language processing AI engines to produce groundbreaking results. However, these applications do not scale well in terms of power and required computing resources\cite{9782767}. There has been an increasing need to find alternative ways to exploit the computation of these highly parallel tasks. To achieve behavior or performance that is comparable to that of the brain, an alternative computing approach is required. The area of computation which aims to achieve this is called neuromorphic computing.


 The major choices of hardware when designing a custom system are digital, analog, or mixed hardware. The commonly adopted digital hardware approaches to simulate neuron dynamics are \ac{CPU}, \ac{FPGA} and \ac{ASIC} based implementations. SpiNNaker\cite{6330636} is an example of multi-core \ac{CPU} based implementation that simulates neurons in an event-driven fashion. \ac{FPGA}'s programmability, and shorter design cycles for different applications enable easier changes for optimizing specific problems. While ASICs are commonly regarded for their low-power consumption and higher performance due to fine-grained control over synthesized hardware, the designs made for \acp{ASIC} are fixed and cannot be altered after the hardware is available. Most ASIC-based implementations are suited to handle only a fixed neuron model.

Recent architectures like Loihi 2 \cite{2111.03746} and spiNNaker2 \cite{1911.02385} are capable of simulating fully-programmable neurons. SpiNNaker2 shows an improvement over the original SpiNNaker architecture by replacing software tasks such as exponential functions and random number generators with dedicated hardware implementations. Loihi 2 can achieve breakthrough performance with 1 million fully-programmable neurons per chip and graded spike events by utilizing special routing techniques, storage and the dedicated Lava software framework which maps neuron models to neuromorphic hardware.


%\begin{figure}[htb!]
%	\centering
%	\input{bilder/ActivationFunction-Pareto.tex}
%	\caption{This is an example which was produced by matlab2tikz.}
%\end{figure}


 The Human Brain Project \cite{MARKRAM201139} aims to establish theoretical foundations for the functioning of the brain. Neuroscience could benefit from brain simulations as they hold promise for improving our understanding of behavior and facilitating the treatment of brain diseases. The ability to simulate multiple neuron models using the same system would be of great use in comparing different neuron models and their behavior in simulating brain functions. Such a diverse population of neurons would be very similar to the behavior observed in different parts of the brain.

  Neuromorphic computing systems aim to solve huge learning tasks faster and in a power-efficient manner inspired by those characteristics of the human brain. To model such a system with parallelism, the low memory bandwidth and serial nature of the \acs{CPU} in Von Neumann architecture is a huge limiting factor. Neuromorphic systems show promising growth in avoiding the end of Moore's law and Dennard scaling.


\section{Research problem}
Highly efficient designs of neuron models are possible when designing for specific models that have a closed-form solution. Most neuromorphic systems utilize \ac{LIF} neurons as they are easier to implement and are computationally efficient and also provide satisfactory results in performing common learning tasks using \acp{SNN}\cite{9782767}. However, to simulate a more accurate and better representation of biological neurons, it is necessary to develop a hardware capable of running biologically-plausible models like Hodgkin-Huxley \cite{sp004764}. Simulation of \ac{COBA}\cite{meffin_burkitt_grayden_2004} synapse models introduces more complexity in simulating the neuron states. These are represented as a set of nonlinear \acp{ODE} and do not contain a closed-form solution. Hence, simulating these neurons is a complex computational task. Designing a hardware that is capable of handling a variety of such models while also maintaining speed and area efficiency is a major challenge. In addition to satisfying diverse neuron requirements, the design must be scalable and readily adaptable to a larger system. Trying to develop a generic processing core while attempting to reach the performance of a dedicated neuron processing core poses a significant challenge.

While performing a sequence of mathematical operations, data dependencies and branches might lead to unpredictable execution length of the algorithm. Finding the right set of code optimizations for the designed processor is essential for a good utilization of the available hardware. Creating a high throughput and high clock speed design is important for reducing the overhead on routing aspects of the external system to which this processing core will be connected.

In summary, this thesis aims to create, evaluate, and execute a parallel processing unit that achieves a balance between flexibility, scalability, resource utilization, and speed.

\section{Methodologies}
Our design is developed based on an \ac{FPGA}-based neuron core which can be incorporated into the already available system in the  Chair of Integrated Digital Systems and Circuit Design at RWTH. This project uses a Xilinx Virtex 7-based \ac{FPGA} board as its platform. Different configurations for the design are considered and simulated. We then find the major drawbacks present in such design and then make improvements on that.

In addition to unit testing of individual components, an assembler has to be custom-made in order to perform behavioral verification on instructions and test the entire design before implementing it on hardware. The encoding scheme of the instruction set is stored in XML notation. The overall system should be comparable to the dedicated HLS model which is capable of high throughput with short clock cycles. Keeping such constraints in mind, we constantly maintain our design to provide more flexibility while maintaining faster operation. The design is iterated keeping the instruction requirements in mind. If the hardware doesn't match the required performance for the target instruction, the required changes are made in the instruction set. Throughout the design, the required assembly code and the performance of realized hardware is monitored to make alterations to our design and instruction set.

 A group of neurons are initiated in both the environments and simulated for a number of timesteps. The processor's result is compared for correctness for a selected neuron and synapse model using the NEST simulator as a reference. Their states and spike emission times are compared.

\section{Document Outline}
Apart from this introductory chapter, this thesis is divided into five chapters. In this introduction, the motivations behind this work are discussed.

Chapter 2 provides the necessary background regarding neuron models and introduce their computational requirement. A brief information about popular designs that are currently used in neuromorphic systems and the approaches used in those designs are shown. 

Chapter 3 presents an approach to solve the problem and the reasoning to choose this particular methodology.

In Chapter 4, the initial design, successive changes during the design cycle and the challenges faced to reach the final implementation are discussed. The improvements and trade-offs based on changing designs and the required program are also discussed here.

The available hardware is now adapted to obtain optimum results based on the proposed methodology. In Chapter 5, we compare the performance of our hardware with that of existing designs and quantitatively measure the design trade-offs.

As with any new approach, there is scope for improvement and learning. Hence, the limitations, future outlook and the potential extensions to the design which can make this work a much more efficient solution towards the goal is presented in Chapter 6.



