%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%															 %
%	IDS main document for bachelor and master thesis		 %
%	created by Jan Richter-Brockmann 						 %
%	Sommersemester 2017										 %
%															 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[
	a4paper,
	12pt,
	twoside=true,
	BCOR10mm,					% Bindekorrektur
	toc=bibliography,			% Fuegt das Literaturverzeichnis ins Inhaltsverzeichnis ein
	cleardoublepage=empty
]{scrbook}

\usepackage[utf8]{inputenc}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%															 %
%	Conditionals											 %
%															 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{ifthen}
\newboolean{Confidential}
\newboolean{EnglishVersion}
\newboolean{Logo}
\newboolean{PrintedVersion}

% Edit this file to set your personal information
\input{settings.tex}

\ifthenelse{\boolean{EnglishVersion}}{
	\usepackage[english]{babel}
}{
	\usepackage[ngerman]{babel}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%															 %
%	Page layout												 %
%															 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[headsepline]{scrlayer-scrpage}
\pagestyle{scrheadings}
\setheadsepline{0.5pt}

\usepackage[onehalfspacing]{setspace}
\usepackage{microtype}	% Nicer spacings, prevents to cross the page's margin


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%															 %
%	Images, formulas and tables								 %
%															 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{subfigure}	
\usepackage{pgfplots}
\usepackage{graphicx}
\usepackage{tikz}	% this package become very handy when plotting graphs 
\usepackage{tikz-timing}
\usepackage{circuitikz}
\usepackage{listings}
\usepackage{xcolor}
\usetikzlibrary{external}
\usetikzlibrary{patterns}
\usetikzlibrary{shapes, arrows}
\usetikzlibrary{circuits.logic.IEC,calc}
\tikzexternalize[prefix=tikzplot/]
\usepackage{xintexpr}

\usepackage[tbtags]{amsmath}
\usepackage{amsfonts}
\usepackage{trfsigns}	% Laplace symbol 
\usepackage{gensymb}

\usepackage{wrapfig}
\usepackage{lscape}
\usepackage{rotating}
\usepackage{epstopdf}

\usepackage{array}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{longtable}
\usepackage{bytefield}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.95}

\lstdefinestyle{CStyle}{
	backgroundcolor=\color{backcolour},   
	commentstyle=\color{codegreen},
	keywordstyle=\color{magenta},
	numberstyle=\tiny\color{codegray},
	stringstyle=\color{codepurple},
	basicstyle=\ttfamily\footnotesize,
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2
}



% Colors - use plot1 - plot7 for your plots
% IDSblua can be used for figures
\definecolor{IDSblue}{rgb}{0,0.3294118,0.623529}%
\definecolor{plot1}{rgb}{0,0.3294118,0.623529}%
\definecolor{plot2}{rgb}{1,0,0}%
\definecolor{plot3}{rgb}{0.647058824,0.647058824,0.647058824}%
\definecolor{plot4}{rgb}{0.498,0.788,0.498}%
\definecolor{plot5}{rgb}{0.745,0.682,0.831}%
\definecolor{plot6}{rgb}{0.941,0.007,0.498}%
\definecolor{plot7}{rgb}{0.749,0.357,0.09}%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%															 %
%	Unities													 %
%															 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[load-configurations=binary, load-configurations=abbreviations]{siunitx}
\ifthenelse{\boolean{EnglishVersion}}{
	\sisetup{group-separator = {,}}
}{
	\sisetup{group-separator = {.}}
}
\sisetup{group-minimum-digits = 3}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%															 %
%	References												 %
%															 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[style=numeric-verb, backend=bibtex8]{biblatex}
\addbibresource{Ref.bib}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%															 %
%	Misc													 %
%															 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[hidelinks]{hyperref}	% Enables links in the PDF
\usepackage[printonlyused]{acronym}
\renewcommand*{\acffont}[1]{{\color{black}\itshape{#1}}}
\usepackage{pdfpages}
\usepackage{algorithm}				% see algorithm documentation to use this package
\usepackage[]{algpseudocode}
\usepackage{pdflscape}
\usepackage{algorithmicx}
\usepackage{algpseudocode}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%															 %
%	Main-Document											 %
%															 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
	\input{title_center.tex}
	\cleardoubleemptypage
	
	% ------------------------------------------------------------------------------------
	% RWTH Assertion
	\tikzexternalenable
	\includepdf[pages=-,scale=1]{appendix/EidesstattlicheVersicherung.pdf}
	\tikzexternaldisable
	\cleardoubleemptypage
	
	\pagenumbering{roman}	% The first few pages are numbered by roman numbers
	% Custom Assertion
%	\input{assertion.tex}	
%	\cleardoubleemptypage
	\phantomsection 		% is required to neglect this chapter in the TOC
	
	% ------------------------------------------------------------------------------------
	% Abstract 
	\ifthenelse{\boolean{EnglishVersion}}{
		\addcontentsline{toc}{chapter}{Abstract}
	}{
		\addcontentsline{toc}{chapter}{Zusammenfassung}
	}	
	\input{abstract.tex}
	\cleardoubleemptypage
	
	% ------------------------------------------------------------------------------------
	% If you would like to use an Acknowledgment, uncomment this section 
	\ifthenelse{\boolean{EnglishVersion}}{
		\addcontentsline{toc}{chapter}{Acknowledgment}
	}{
		\addcontentsline{toc}{chapter}{Danksagung}
	}	
	\input{acknowledgment.tex}
	\cleardoubleemptypage
	
	% ------------------------------------------------------------------------------------
	% Table Of Contents
	\tableofcontents
	\cleardoubleemptypage
	\phantomsection			% is required to neglect this chapter in the TOC
	
	% ------------------------------------------------------------------------------------
	% List of Figures
	\addcontentsline{toc}{chapter}{List of Figures}
	\listoffigures
	\cleardoubleemptypage
	
	% ------------------------------------------------------------------------------------
	% List of Tables
	\addcontentsline{toc}{chapter}{List of Tables}
	\listoftables
	\cleardoubleemptypage
	
	% ------------------------------------------------------------------------------------
	% Abbreviations
	% Insert your abbreviation in Abbreviations.tex - to get a sorted list, use the command below and you will get Abbreviations-sort.tex which is actually included
	\chapter*{Abbreviations}
	\addcontentsline{toc}{chapter}{Abbreviations}
	\begin{acronym}[ABCDEFGH]
		\setlength{\parskip}{0ex}			% Reduce the line spacing in the abbreviations
		\setlength{\itemsep}{1ex}
		\input{Abbreviations-sort.tex}		% To sort the abbreviations, us the follwing command: sort Abbreviations.tex /O Abbreviations-sort.tex
	\end{acronym}
	\cleardoubleemptypage
	
	\pagenumbering{arabic}	% change the numbering to arabic numbers
	
	% CONTENT - include all your content files in content.tex
	\input{content.tex}

\end{document}
