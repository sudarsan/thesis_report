\chapter{Background and related work}

In order to design a generalized neuron processing core, we need an understanding of neuronal dynamics and their adaptation to simulate biological neurons and to implement \acp{SNN}. Neurons of an \acs{SNN} have more biologically plausible characteristics compared to \ac{ANN}. The distinct feature of an \ac{SNN} is to transmit information in the form of spikes or action potentials. There are also SNN models that use rate-based encoding schemes where the information is not transmitted as spikes but as spike rates. \ac{ANN} computations are dominated by matrix-vector multiplications and convolutions. A number of accelerators such as the \ac{NPU}\cite{esmaeilzadeh2012neural}, \ac{TPU}\cite{jouppi2017datacenter} and \ac{ReRAM} based designs have produced massive improvements. The required adaptations for \acp{SNN}\cite{CHEN2020264} are much more complicated and hence the available implementations are also not as effective. Based on the design problem, neuromorphic systems may be classified into two areas: systems for neuroscience and systems for machine intelligence\cite{9782767}.

\section{Neuron model}

The human brain contains an estimated 100 billion\cite{herculano2012remarkable} fundamental units called neurons. A neuron can be divided into three functional parts called dendrites, soma, and axon. Soma is the cell body which is enclosed by a membrane which can be conceptualized as a processing device with multiple inputs and a single output. When the total of incoming inputs to the soma reaches a particular threshold, a signal is generated and sent out as output. The dendrites acts as the input device which collects the signals from other neurons and transmits them to the soma. This output is sent to external neurons by the axon. The junction between two neurons is called a synapse and the information is transmitted by through these junctions. The neuron which sends the signal is called as presynaptic cell and the one receiving it is the postsynaptic cell. The neuronal signals are in the form of short electrical pulses called action potentials or spikes. These spikes have an amplitude of 100 mV and with a duration of 1 to 2 ms\cite{epflbook}.

In an \ac{SNN} model, neurons are interconnected to each other through the synapses. The neurons acts as information processing elements and the synapses facilitate learning and transmission of information. The membrane potential of a neuron without receiving any input spike stays at a stable value called as resting potential $u(t) = u_{rest}$. When in a resting state, the cell is already polarized with a significant negative potential of approximately -70 mV. At $t = 0$, when the presynaptic neurons fires a spike, at t $>$ 0, the postsynaptic neuron's response is given by $ u(t) = u_{rest} + \epsilon(t)$ where $\epsilon(t)$\cite{epflbook} is the postsynaptic potential (PSP). If $u(t) - u_{rest} > 0$ it is called as excitatory postsynaptic potential (EPSP), otherwise, it is called inhibitory postsynaptic potential (IPSP). When there are multiple presynaptic neurons j=1,2,.. sending spikes to postsynaptic neuron i, each creates a postsynaptic potential $\epsilon_{i1}, \epsilon_{i2}$ and so on. The total change in potential is sum of individual PSPs.
\begin{equation}\label{key}
	u_i(t) = \sum_{j} \sum_{f} \epsilon_{ij}(t-t_j^{(f)}) +  u_{rest}
\end{equation}
where $t_j^{(f)}$ is the firing time of the presynaptic neuron j.
As soon as the membrane potential reaches a critical value $u_{th}$, the neuron emits a pulse with an amplitude of about 100 mV. The threshold value ($u_{th}$) for spike initiation is about 20 to 30 mV above the resting potential \cite{epflbook}. This pulse propagates along the axon of neuron $i$ to the synapses of other neurons. After the pulse, the membrane potential does not return to the resting potential but it reaches a hyperpolarized state below resting value.

\begin{center}
	\begin{figure}
	\includegraphics{bilder/action_potential.jpg}
	\caption{Behavior of membrane potential with time. The membrane potential depolarizes initially, then reaches threshold and then repolarizes. It then finally returns to rest.}
\end{figure}
\end{center}


\subsection{Integrate and Fire models}
As the neuron action potentials always have a regular form, it doesn't carry any additional information. Only the time of the spike carries information and hence these signals can just be represented as events that occur at a particular instant. The neuron models where these information signals are represented as events are called integrate and fire models. An integrate and fire model can be represented using (1) a linear differential equation to describe membrane potential characteristics and (2) a threshold for spike\cite{epflbook}.

The behavior of neuron can modeled as a parallel RC circuit with external current $I(t)$. This is due to the insulating properties of the cell membrane which acts as a capacitor and the leakage current which is represented by a resistor. Hence, The leaky integrate-and-fire model is defined by the differential equation, 
\begin{equation}
	\tau_m\frac{du}{dt} = -[u(t)-u_{rest}] + RI(t) \text{;   } \tau_m=RC 
\end{equation}

Assuming the initial condition $u(t_0) = u_{rest} + \triangle u$, the membrane potential is described as

\begin{equation}\label{key}
	u(t) - u_{rest} = \triangle u \exp{- (\frac{t-t_0}{\tau_m})}
\end{equation}
Thus, when there is no external input, the membrane potential decays exponentially to its resting value.

The firing time is the moment at which a neuron emits an action potential, i.e, $t^{(f)}: u(t^{(f)}) = u_{th}$ . The firing time is however defined as the time after the potential is reset to a new value $u_r < u_{th}$. 
	\begin{equation}
		\lim\limits_{\delta \to 0; \delta > 0} u(t^{(f)} + \delta) = u_{rest}
	\end{equation}
After the spike emission, the membrane potential is reset to the value $u_{rest}$.

\subsection{Hodgkin-Huxley model}
The Hodgkin-Huxley model is a more detailed model which accounts for different types of neuron, synapse and geometry of an individual neuron. The experiments performed by Hodgkin and Huxley found three types of ion currrent i.e, sodium, potassium and leak current \cite{hodgkin1990quantitative}. The leak current represents other channel types which are not described explicitly. Specific voltage-dependent ion channels for sodium and potassium control the flow of the ions through cell membrane. 
The applied current $I(t)$ is split into three channels specified above and it charges the capacitor $C$.
\begin{equation}
	C\frac{du}{dt} = I(t) - \sum_{k}I_k(t)
\end{equation} 
The sum of three ionic currents satisfy the following:
\begin{equation}
	\sum_{k} I_k = g_{Na}m^3h(u-E_{Na}) + g_Kn^4(u-E_K) + g_L(u-E_L)
\end{equation}
where $E_{Na}$, $E_K$, and $E_L$ are reversal potentials. The effective conductance of potassium channel is $1/R_K =g_Kn^4$ and $n$ represents the activation of the channel\cite{epflbook}. For the sodium channel, $m$ represents the activation and $h$ corresponds to the inactivation. Since there is no closed form solution for this model represented by ordinary differential equations, the behavior of this neuron can be realized by using numerical solvers. The compute requirements to simulate Hodgkin-Huxley is much greater in comparison to integrate and fire models.

\section{Synapse models}
\subsection{Conductance-based synapse}
In a \ac{COBA} synapse model\cite{connor1971prediction}, the input current of a neuron $i$ is the sum of contributions from different neurons $j$ with transmitted spikes at time $k$.
\begin{equation}
	I_i = \sum_{j} I_{ij} = \sum_{j} \sum_{k} g_{ij}f_{ij}(t-t_j^k)(u_i-E_{ij}) + I_{ext}
\end{equation} where $g_{ij}$ is a function which represents conductance between neuron $i$ and neuron $j$, $f_{ij}(t-t_j^k)$ is a function which represents the change in conductance with time and $E_{ij}$ is the reversal potential between neuron $j$ and neuron $i$. The input current is represented as a function of potential across the channel and the conductance.
The synaptic conductance is the total of all the conductances along with its temporal behavior $f(t-t_j^k)$ and is modeled by
\begin{equation}
	g_i(t) = \sum_{j} g_{ij}f({t-t_j^k})
\end{equation}

\subsection{Current-based synapse}
In \ac{CUBA} synapses, the strength of the current is directly proportional to synaptic weight. The total synaptic current at postsynaptic neuron $i$ is given by
\begin{equation}
	I_{syn,i} = \sum_{j} s_{ij}w_{ij}
\end{equation}
where $w_{ij} $ is the synaptic weight between the presynaptic neuron $j$ and postsynaptic neuron $i$ and $s_{ij}$ is 1 if the presynaptic neuron is spiking. The weight attribute can vary based on excitatory or inhibitory behavior of the synapse.

\section{Numerical solvers}
Although less complex neuron models can be solved using closed-form solutions, many biologically accurate models do not have a precise solution. In certain situations, the process of obtaining an analytical solution can be very time-consuming. Hence, we have to adopt numerical integration methods. For practical purposes, a numeric approximation is sufficient. The numerical methods to solve first-order initial value problems can be classified into implicit and explicit methods. Explicit solvers use the precomputed grid points to calculate the next step. They are comparatively less stable to implicit methods but it is computationally cheaper. The most commonly used explicit numerical algorithms are Euler's method\cite{braun1983differential} and \ac{RK} method\cite{butcher1996history}. 



From any point on a curve, the approximation of a nearby point can be found by moving a short distance along the tangent to the curve. For a differential equation of the form $y'(t)=f(t,y(t))$, we choose a step value $h$ and the solution is
\begin{equation}
	y_{n+1} = y_n + hf(t_n,y_n) \text{ and }   y_n = const.
\end{equation}


Runge-Kutta method is a generalized form of Euler's method. The next timestep is given by $y_{n+1}=y_n+h\sum_{i=1}^{a}b_ik_i$ where $b_i$ is the weight distribution coefficient and $k_i$ represents the slope of the differential equation from the explicit start point to different intervals within the next timestep. These values are calculated in a step-wise manner. Hence, the presence of finer approximations in higher-order \acs{RK} methods helps compute a more numerically accurate solution.

 An improvement regular \ac{RK} can be performed using adaptive step size $h$. In this approach, the step size is adjusted to reach an accuracy goal which is measured in terms of truncation error. This additional overhead in calculating error is compensated by the accuracy it provides. RK-45(Runge-Kutta-Fehlberg) method is the preferred adaptive numerical integration method adopted for solving \ac{ODE} by our reference framework, the NEST\cite{Gewaltig:NEST} simulator.

\section{Software pipelining}
Software pipelining\cite{lam1988software} is a static instruction scheduling method which is used to utilize processing units that can compute multiple instructions simultaneously. It executes instructions from different iterations of a loop in the same pipeline. This make the pipeline to be busy with minimal stalls. 

It is a combination of loop unrolling and compression of operations from successive iterations to make maximum utilization of the hardware. The target code must contain prologue, kernel and epilogue. The kernel is the active part of the software-pipelined loop. Number of cycles within the kernel is called \ac{II}. It is equivalent to how quickly the next iteration of the loop can start. Finding the smallest II is an NP-Complete problem\cite{hsu1986highly}. Dependency constraints and resource constraints are the main factors in deciding the optimum \ac{II}. The common approach to perform software pipelining are Iterative modulo scheduling\cite{rau1996iterative} and slack scheduling\cite{huff1993lifetime}. The final code structure of a software pipelined loop is as in \figurename{ \ref{swpipeline}}. The processor spends most of its time in the kernel stage and thus ensuring a higher utilization of available hardware.

\begin{center}
	\begin{figure}[!ht]
		\includegraphics[scale=0.4]{bilder/swpipeline.png}
		\caption{The execution flow of a regular loop and a software pipelined loop. Loop-carried dependencies execute operations of different iterations within the kernel.}
		\label{swpipeline}
	\end{figure}
\end{center}

\section{Related Projects}

\subsection{NEST simulator}
The NEural Simulation Tool(NEST) \cite{Gewaltig:NEST} is a tool designed to simulate large heterogeneous networks of point neurons. It has an interface to Python while the kernel is written in C++. A Python-based user interface called PyNEST\cite{eppler2009pynest} can be used to initiate the simulation which invokes the required neuron model present in the kernel. It is built to handle multi-threading and also parallelism to be exploited in computer clusters and supercomputers.

 A diverse set of over 50 neuron and 10 synapse models along with different variants of \ac{STDP}\cite{caporale2008spike} is supported. It provides support for simple integrate-and-fire neuron models with current based synapses to complex models like Hodgkin-Huxley\cite{hodgkin1990quantitative}. Additionally, it supports the creation of custom neuron and synapse models with the help of NESTML\cite{nestml}, a domain-specific language to model neurons. Based on the neuron model, the simulation is performed using exact solution whenever available, otherwise, numerical integration is performed.
\subsection{Loihi 2}
Intel Labs launched the Intel Neuromorphic Research Community and released the Loihi processor\cite{8259423}. Building on the insights gained from the research performed on the Loihi chip, Intel Labs introduced Loihi 2\cite{orchard2021efficient}. It is an \ac{ASIC} based system which can handle a maximum of 1 million fully-programmable neurons per chip and about 120 million synapses.The system provided support for self-learning capabilities, new models of neurons, and communication based on asynchronous spikes. The target neuron model is defined by a sequence of microcode instructions. The microcode supports bitwise and basic arithmetic and conditional operations on branching \cite{2111.03746}. Loihi2 has a programmable pipeline in each neuromorphic core to support arithmetic, compare and program control flow instructions. As Loihi 2 is programmable, it can implement a wide range of neuron models. It is able to handle such diversity without compromising on the performance due to optimizations in memory utilization and I/O interfaces. Loihi 2 allows graded spike events as an improvement from Loihi which only supported binary spike events. 
\subsection{The SpiNNaker project}
SpiNNaker\cite{furber2014spinnaker} is a highly parallel computing engine that enables the simulation of large-scale \acp{SNN} with up to a billion neurons and a trillion inter-neuron connections, in real-time. It is a customizable platform that allows neuroscientists and brain researchers to study brain functions using software neuronal models.

The SpiNNaker system has an array of processing nodes. Each node contains a set of local ARM processing cores which is connected to the packet router which transmits the spikes based on the source. Within the node, one of the core is selected as monitor processor which handles operating system support tasks while the other nodes process the application\cite{furber2012overview}. In order to have a smoother packet transmission, mechanism like packet dropping are adopted. SpiNNaker is a fault-tolerant system which is designed to work after such information loss. Still, the occurence of faults leads to unpredictable results which is handled by limiting the speed of execution such that the dropped packets can be recovered\cite{furber2020spinnaker}.

\subsection{FPGA-based designs}
\ac{FPGA}-based systems are primarily known for their flexibility.  \ac{FPGA} implementations can be used as an intermediate solution on the way to a custom chip implementation\cite{schuman2017survey}. In such cases, the programmability of the FPGA is not fully utilized. FPGA implementations of SNNs are limited by finite FPGA resources and designers are required to make trade-offs between resource utilization, speed and accuracy. In order to tackle the problem of limited hardware resources, most \ac{FPGA}-based architectures contain multiple FPGAs along with specialized routing \cite{7045812}. If the goal of the design is to achieve very high performance at low power consumption, \ac{FPGA}s aren't the most suitable option. A few example implementations include: implementation of a taste recognition using an evolving integrate and fire model \cite{soltic2008evolving}, \ac{STDP} inference for convolutional \ac{SNN} on \ac{FPGA}\cite{yousefzadeh2017hardware}, object recognition using spiking deep convolutional neural networks\cite{cao2015spiking}. In general, due to their wide availability, shorter design cycles, ability to be re-programmed, and ease of interfacing with hosts, they are commonly utilized to test a diverse array of models and algorithms under the scope of neuromorphic computing.

\section{Flynn's taxonomy}
Flynn's taxonomy\cite{1447203} classifies computer architectures based on the number of concurrent instruction streams and data streams available in the architecture. They are classified as:

\ac{SISD}: These computers have no parallellism in instruction or data. They perform single operation on a single processing element on a single data stream. The first personal computers and mainframe computers were of this type. The design is simple, easy to program but it has a slow execution rate and data processing speed which makes it not ideal for our purposes.

\ac{SIMD}: A single instruction is simultaneously applied on multiple data streams on multiple hardware processing elements. Array processor and vector processors are a form of SIMD machines. They can be used in cases where the same set of operations have to be performed on a large data. \cite{5009071}.

\ac{MISD}: Different types of systems which receive the same data and perform the same operation should provide the same result. They are used for fault tolerant applications where the correctness is essential. These systems are of no concern with regards to our scenario as we need a lot of data to be operated in parallel. Adding redundancy for fault tolerance with regards to computation accuracy isn't of much significance in our scenario\cite{duncan1990survey}.

\ac{MIMD}: These processors can simultaneously execute multiple instructions on different data. Modern multi-core systems which execute multiple unrelated and computationally distinct operations in parallel are under this class of computer architecture. Modern processors have multiple execution units and can execute more than one instruction per clock cycle (\ac{IPC} $>$ 1). These processors are called as superscalar processors. Dynamic out-of-order superscalar processors uses complex hardware to reorder instructions and also resolve  dependencies during runtime\cite{shen2013modern}. Static multiple-issue processors(also called \ac{VLIW} processor) are much simpler parallel computing design which rely heavily on the static scheduling of instructions.

Overall, the paradigms within Flynn's taxonomy which are of our interest are \ac{SIMD} and \ac{MIMD}. These designs are capable of handling more than one data and output the results of independent operations at a faster rate.

