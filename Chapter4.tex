\chapter{Implementation details}
During the design phase of our processor, there were multiple incremental changes, however, it can viewed as two designs with distinct features.

The set of instructions a VLIW processor operates in parallel is collectively called a \textit{bundle}. The total number of parallel operations present inside a bundle can vary based on the application requirements and architecture. Each sub-instruction within a bundle is called a \textit{slot}. 

This work experiments with 4 instructions in parallel and the sub-instructions are named slot 0 to slot 3. This is an arbitrary choice selected for this design and the example neuron model used is the exponential \ac{IAF} neurons with \ac{CUBA} synapse \cite{Gewaltig:NEST}. The C code to simulate a single timestep of this neuron is shown in Listing \ref{lst:example}.


\lstset{backgroundcolor=\color{gray!10}, frame=single, style=CStyle}
\lstset{basicstyle=\ttfamily\footnotesize,breaklines=true} % Set the font and allow line breaks
\lstset{numbers=left, numberstyle=\tiny, stepnumber=1, numbersep=5pt, captionpos=b} % Add line numbers
\begin{lstlisting}[language=C,caption={C code for Single timestep of CUBA exponential IAF neuron}, label=lst:example]
int iaf_psc_exp_timestep(float Vm[], int tref[], float i_syn_inh[], float i_syn_exc[], int weight_inh[], int weight_exc[], float I_ext[]){
int num_neurons = 512;
const float P22 = 0.99004;
const float P21_exc = 0.0004;
const float P21_inh = 0.0004;
const float P20 = 0.0004;
const float P11_exc = 0.95122;
const float P11_inh = 0.95122;
const float weight_frac_exc = (float)1/(1 << 16);
const float weight_frac_inh = (float)1/(1 << 16);
const float V_th = 15.0;
const V_reset = 0.0;
const int refractory_period = 20;
float weighted_input_inh;
float weighted_input_exc;
int spike;
for(int i=0; i<num_neurons; i++){
	spike = 0;
	//Voltage update
	//refractory check
	if(tref[i] == 0){
		Vm[i] = Vm[i]*P22 + i_syn_inh[i]*P21_inh + 
		i_syn_exc[i]*P21_exc + I_ext[i]*P20;
	}else{
		t_ref[i] = t_ref[i] - 1;
	}
	if(Vm[i] > V_th){ //Threshold check
		Vm[i] = V_reset;
		t_ref[i] = refractory_period;
		spike = 1;
	}
	// Current update
	weighted_input_inh = weight_frac_inh*weight_inh[i];
	weighted_input_exc = weight_frac_exc*weight_exc[i];
	i_syn_exc[i] = i_syn_exc[i]*P11_exc + weighted_input_exc;
	i_syn_inh[i] = i_syn_inh[i]*P11_inh + weighted_input_inh;
	}
}
\end{lstlisting}

\section{CPU design I}
The initial design consists of a 32-bit register file with 32 registers and hence requires 5-bit register encoding. The processor handles float, integer and logical operations. The data memory contains 32 bits per address line and is addressable by indexed addressing where the address is calculated by adding a base address to some offset. The address increment is by 1 for a line as there can be only a single 32-bit value stored per line. This memory scheme is chosen because there are no supported operations on half-word or a byte. The data address is of 12 bits which is chosen based on the estimate that it can hold approximately 512 neurons containing eight state variables. Even though the design is adaptable to handle various programs, the number and type of functional units are optimized for \ac{IAF} neurons model with CUBA synapses.The slots are capable of handling sub-instructions with similar encoding to enable easy extension to additional slots if required for simulating a neuron model which would benefit from additional parallel units. The instruction width is 10 bits wide(1024 rows, 4-wide) based on the assumption that a neuron model's code doesn't exceed this limit. The instruction and data can be initialized through \acs{AXI} memory-mapped interface.


The computation units required by the processor was decided based on the simulation requirements. Arithmetic operations like addition, subtraction, floating point addition and multiplication are necessary aspects of neuron computation. In addition to these basic operations, the presence of bitwise logical operations and shifts can help with reducing the computation times. After checking for the timing criteria, there is a benefit of two cycles in utilizing multiply-accumulate operation. However, it is not adopted due to the additional register operand and this additional port remains unused for other operations and hence remains redundant for the increased fan-out of the register file it causes. The resource allocation of each of the instruction and its slot is as specified in \tablename{ \ref{table:cpu1slots}}. A dual port data memory is used so that two reads and writes are possible by the slots 3 and 4.

 To obtain the minimum latency of all the operations, the timing requirement of each operation was checked within the ALU module for the target FPGA. The minimum satisfactory latencies for 200 MHz frequency are as shown in \tablename{ \ref{table:aludelays}}. Due to the high latency, \code{divf} instruction should be avoided for constants. They can be replaced by multiplication of the reciprocal. \code{divf} unit is not available in this design as we can replace it by multiplication. However, if a future implementation requires a floating division operation, it is possible to integrate it by just creating an additional port within the ALU. Floating-point neuron parameters and exponentials are often found in the solutions of differential equations. Computation of these parameters can also be avoided by storing the pre-computed values directly in data memory. To reduce the hardware within the microarchitecture, the arithmetic units are reused for 1)\code{addf} and \code{subf}, 2)\code{cmpflt}, \code{cmpfeq} and \code{cmpfge}, 3) \code{add} and \code{sub} and 4) \code{cmplt}, \code{cmpeq} and \code{cmpge}.
 
This design contains multiple instructions running at different pipeline depths:
\begin{itemize}
	\item Arithmetic and logical operations which take single cycle to execute follow a 4-stage pipeline with Instruction fetch\,(F), Register read\,(R), ALU\,(E) and register writeback(W) stage. Example: \code{add}, \code{sub}, \code{shr} etc.
	\item Memory load uses a five stage pipeline with additional memory read\,(M) in between the E and W stage. 
	\item The \code{wb} instruction requires only two stages, Instruction Fetch(F) and register writeback(W).
	\item The compare operations are handled in a 4 stage or 5(2 execute stages for float comparison) stage pipeline with the results directly written back to predicate registers. These registers are contained within the Control Unit and are available during the register read(R) stage of the pipeline.
\end{itemize}

\begin{center}
	\begin{table}[h!]
		\centering
		\begin{tabular}{ | c | c | c | c | c |}
			\hline
			Instruction & Slot 0 & Slot 1 & Slot 2 & Slot 3  \\ [0.5ex] 
			\hline\hline
			
			\code{add, sub} & \checkmark & \checkmark & \checkmark & \checkmark \\   
			\code{addf, subf, mulf} & \checkmark & \checkmark & \checkmark & \checkmark \\
			\code{cmp (cmpge, cmplt, cmpeq)} & \checkmark & \checkmark & \checkmark & \checkmark \\
			\code{cmpf (cmpfge, cmpflt, cmpfeq)} & \checkmark & \checkmark & \checkmark & \checkmark \\
			\code{jmp} & \checkmark & - & - & - \\
			\code{and, or, xor} & \checkmark & \checkmark & - & - \\
			\code{shl, shr} & \checkmark & \checkmark & - & -\\
			\code{ld, st} & - & - & \checkmark & \checkmark \\
			\code{spike} & - & \checkmark & - & - \\
			\code{wb} & \checkmark & \checkmark & \checkmark & \checkmark \\
			\hline 
		\end{tabular}
		\caption{Instructions supported by the slots.(the immediate operations are also supported by the same slots that have the respective units)}
		\label{table:cpu1slots}
	\end{table}
\end{center}


\begin{center}
	\begin{table}[h!]
		\centering
		\begin{tabular}{ | c | c |}
			\hline
			Instruction & latency(cycles)  \\ [0.5ex] 
			\hline\hline
			
			\code{cmpf} & 2 \\   
			\code{addf, subf} &  4 \\
			\code{mulf} & 4 \\
			\code{divf} & 8 \\
			\code{itof} & 2 \\
			\text{Other operations} & 1 \\ 
			\hline 
		\end{tabular}
		\caption{Latency of operations. Load and store latencies are for the entire path}
		\label{table:aludelays}
	\end{table}
\end{center}


Hazard detection units are absent and it is the concern of the programmer to schedule the instructions considering by inserting \code{nop}s in code when required. A writeback(\code{wb}) instruction performs the write to the register file by directly forwarding the pre-computed results from the ALU. It is important to note that in CPU design I, \textbf{only} long latency instructions can be handled by the \code{wb} instruction. The integer and logical operations are handled without a writeback as these instructions handle their destination register address in the pipeline. For example, an addition operation can be handled independently, using \code{add r0 r1 r2}(r0=r1+r2) without a \code{wb} operation. To perform a similar floating point addition, two separate instructions are required i.e, \code{addf r1 r2} and \code{wb r0 addf}. Please note that there must be a delay of \textit{latency} cycles between the issue of \code{addf} and \code{wb}.


\subsection{Predicated execution}
In each iteration of a neuronal computation, we need to have conditional execution based on refractory condition and the membrane potential reaching the threshold voltage. To handle multiple neurons in the same cycle or having multiple independent operations whose execution depends on different conditions, there is a need for multiple flags to handle those operations. This problem is handled by using storing the results of multiple conditional values into special registers called predicate registers.


The predicate registers\,(\code{p0, p1, p2} and \code{p3} ) are updated by compare operations\,(\code{cmp} and \code{cmpf}) and are initialized to zero by default. The values of these registers are accessible at the instruction decode stage of the pipeline. The execuite stage(E) of integer compare operation takes 1 cycle whereas a floating point compare it takes 2 cycles. Each predicate register correspond to their respective slots 0 to 3, i.e, a compare operation performed on slot 3 will \textit{update} the value in predicate register \code{p3}. Any predicate register can be \textit{used} by any slot to perform conditional operations and conditional writeback operations. 

In this simplified design, all instructions are affected by the applied predicate condition. If the predicate condition is false, the opcode value to the ALU, memory\_enable and register\_write signals are all set to zero and no operation happens in that slot.

\begin{verbatim}
	cmplt r0 r1                          
	if(p0) add rd rs1 rs2
\end{verbatim}

It is possible to use a `!' to negate the required condition. For instance, \code{if(p0)} and \code{if(!p0)} can be used to handle an \code{if,else} statement. This enables any conditional to be used between two operands $a$ and $b$ using three compare operations $a \geq b$, $a < b$ and $a = b$.

\subsection{Instruction set and encoding}
The encoding varies according to the instruction class. The encoding scheme for each class is as shown in \figurename{ \ref{fig:encodingcpu12}}. $rs1$ and $rs2$ are the source register operands and $rd$ is the destination register. $Class$ refers to the instruction class. 4 bits are reserved for handling $Pred$\,(predicated) operations. Bit 3 represents the presence or absence of predicate condition, bit 2 represents the negation of the predicate register's value and bits 0 and 1 are used to index the predicate registers \code{p0} to \code{p3}, so, the encoding ``1101" represents \code{if(!p1)}. The requirement of predication on almost all instructions facilitates the need for $Pred$ bits for all the instruction classes. $opcode$ bits is used to execute an operation in case or Integer/Logical or floating point operation but it is used to obtain the result from the ALU in case of \code{wb} operation.

\begin{figure}
	\begin{center}
		\begin{bytefield}[endianness=big]{32}
			\bitheader{0-31} \\
			\bitbox{4}{Pred} &  \bitbox{5}{} &
			\bitbox{5}{rs2} & \bitbox{5}{rs1} &  \bitbox{5}{rd}
			& \bitbox{4}{opcode}
			& \bitbox{4}{Class}
		\end{bytefield}
		\text{(a)Integer and logical operation}
	\end{center}
	
	\begin{center}
		\begin{bytefield}[endianness=big]{32}
			\bitheader{0-31} \\
			\bitbox{4}{Pred} &  \bitbox{5}{} &
			\bitbox{5}{rs2} & \bitbox{5}{rs1} &  \bitbox{5}{}
			& \bitbox{4}{opcode}
			& \bitbox{4}{Class}
		\end{bytefield}
		\text{(b)Floating point operation }
	\end{center}
	
	\begin{center}
		\begin{bytefield}[endianness=big]{32}
			\bitheader{0-31} \\
			\bitbox{4}{Pred} &  \bitbox{15}{} &
			\bitbox{5}{rs1}
			& \bitbox{4}{opcode}
			& \bitbox{4}{Class}
		\end{bytefield}
		\linebreak
		\text{(c)spike operation }
	\end{center}
	
	\begin{center}
		\begin{bytefield}[endianness=big]{32}
			\bitheader{0-31} \\
			\bitbox{4}{Pred} &  \bitbox{11}{} &  \bitbox{5}{rd}
			& \bitbox{4}{datasrc} & \bitbox{4}{opcode}
			& \bitbox{4}{Class}
		\end{bytefield}
		\linebreak
		\text{(d) Writeback operation}
	\end{center}
	\begin{center}
		\begin{bytefield}[endianness=big]{32}
			\bitheader{0-31} \\
			\bitbox{4}{Pred} & \bitbox{2}{}  \bitbox{11}{offset imm} &  \bitbox{5}{rs}
			& \bitbox{5}{rd} & \bitbox{1}{ld}
			& \bitbox{4}{Class}
		\end{bytefield}
		\linebreak
		\text{(e) Load operation}
	\end{center}
	
	\begin{center}
		\begin{bytefield}[endianness=big]{32}
			\bitheader{0-31} \\
			\bitbox{4}{Pred} & \bitbox{2}{}
			& \bitbox{11}{offset imm} & \bitbox{5}{rbase} & \bitbox{5}{rs} & \bitbox{1}{st}
			& \bitbox{4}{Class}
		\end{bytefield}
		\linebreak
		\text{(f) Store operation}
	\end{center}
	
	\begin{center}
		\begin{bytefield}[endianness=big]{32}
			\bitheader{0-31} \\
			\bitbox{4}{Pred} & \bitbox{6}{} &  \bitbox{5}{rs2(opt)} &  \bitbox{5}{rs1(opt)}
			& \bitbox{4}{sel fp} & \bitbox{4}{opcode}
			& \bitbox{4}{Class}
		\end{bytefield}
		\text{(g) Bypassed float operation}
	\end{center}
	
	\begin{center}
		\begin{bytefield}[endianness=big]{32}
			\bitheader{0-31} \\
			\bitbox{4}{Pred} & \bitbox{9}{} &    
			& \bitbox{11}{target addr} & \bitbox{4}{opcode}
			& \bitbox{4}{Class}
		\end{bytefield}
		\linebreak
		\text{(h) Jump operation}
	\end{center}
	
	\caption{Instruction encoding for CPU I}
	\label{fig:encodingcpu12}
\end{figure}

\begin{itemize}
	\item \textbf{Integer and logical operation}: The operations under this class are \code{add, sub, and, or, xor, shl} and \code{shr}. The shift operations here are arithmetic shift. The instruction is of the form \code{instr rd rs1 rs2}.
	\item \textbf{Floating point operation}: Single precision floating point \code{addf, mulf} and \code{subf}. The instruction is of the form \code{instr rs1 rs2}.
	\item \textbf{Writeback}: The results obtained from the previously initiated floating point are captured and written to destination register. It is of the form \code{wb rd fp\_unit}, where \code{fp\_unit} $\in$ \{\code{addf}, \code{subf}, \code{mulf}\}. It is essential to note that the writeback is possible only on the same slot where the float operation was initiated.
	\item \textbf{spike}: The spike signal is emitted from the processor and along with it, the register containing the neuronID can be specified such that the value can be pushed into a \ac{FIFO} buffer. It is of the form \code{spike reg\_neuronID}.
	\item \textbf{Load/Store}: The memory load(\code{ld}) and store(\code{st}) are performed by indexed addressing. Hence, the base address register, offset and the destination register(for load) must be specified. Example: \code{ld r5 r3 5}. The register r5 is the destination, r3 contains the base address and the integer value 5 is the address offset. The offset value is a signed 11-bit number.
	\item \textbf{Bypassed float}: The float operations can operate on values calculated from previous floating units' result. This instruction avoids the delay caused due to intermediate register storage. Example: \code{mulf\_b r5 addf}.
	\item \textbf{Jump}: The target address is specified by the required label location which is converted to a 11-bit binary code representing the program location.
\end{itemize}

An example code for the voltage update is shown in Listing \ref{lst:cpu1example}. The neuron identifier sent with the spike is represented by the memory location of the neuron's states. One iteration of a voltage update takes 23 cycles.

\lstset{language=Python, backgroundcolor=\color{gray!10}, frame=single} % Set the language of the code block
\lstset{basicstyle=\ttfamily\footnotesize,breaklines=true} % Set the font and allow line breaks
\lstset{numbers=left, numberstyle=\tiny, stepnumber=1, numbersep=5pt, captionpos=b} % Add line numbers
\begin{lstlisting}[caption={Voltage update code block with pre-initialized constants}, label=lst:cpu1example]
#assuming r0 contains neuron state's base address
# P22 -> r7, P_21_exc -> r6, P21_inh ->r5, P20 ->r4
# r0 -> address pointer, r1 -> offset to next neuron
# r3 - V_reset, r31-zero, r15 - V_th, r20 -> 1
# r14 -> num_neurons*neuron_offset+parameter_baseoffset
LOOP:
nop || nop || lw r15 r0 0 || lw r16 r0 1 #load Vm, i_syn_exc
nop || nop || lw r18 r0 2 || lw r17 r0 3 #load tref, i_syn_in
# Vm*P22 , i_syn_exc*P21_exc, load I_ext
mulf r15 r7 || mulf r16 r6 || lw r19 r0 4 || nop 
#(tref==0)? and P21_inh*i_syn_inh
cmpeq r31 r18 || nop || nop || mulf r17 r5 
add r0 r0 r1 || if(!p0) sub r18 r18 r20 || nop || mulf r19 r4 #pointer r0 increment, P20*I_ext
nop || nop || nop || nop
#predicated writing of results
wb r15 mulf || wb r16 mulf || nop || nop
addf r15 r16 || nop || nop || wb r17 mulf
nop || nop || nop || wb r18 mulf
nop || nop || nop || addf r18 r17
nop || nop || nop || nop
wb r15 addf || nop || nop || nop
nop || nop || nop || nop
nop || nop || nop || wb r16 addf
addf r15 r16 || nop || nop || nop
nop || nop || nop || nop
nop || nop || nop || nop
nop || nop || nop || nop
if(p0) wb r15 addf || nop || nop || nop
#Vm > Vth comparison, compare number of neurons executed
cmpflt r7 r15 || cmplt r0 r14 || nop || nop
nop || nop || nop || nop
#inserting Vreset+0 to r15 for spike condition
nop || if(!p0) add r15 r3 r31 || nop || nop
if(p1) jmp LOOP || if(!p0) spike r0 || nop || if(!p0) st r15 r0 -4 #store updated Vm
EXIT:
jmp EXIT || nop || nop || nop
\end{lstlisting}

\subsection{Limitations}
There are two major limitations observed in this instruction set.
The presence of \code{wb} instruction which occupies the entire slot makes it difficult to schedule an arithmetic operation. This means that a floating point unit without utilizing bypassing the result of another float operation occupies two slots in the code. This is an inefficient usage of instruction space and computing capability and consequently makes the implementation of software pipelining algorithm harder. Hence, an enhanced approach which addresses these issues is performed in CPU design II.


 The second limitation is the difficulty in utilizing the bypassed float operations(\code{addf\_b, mulf\_b etc}). Memory width is small which causes each value to be fetched by an individual load. Using a dual port memory offers loading data from two slots 2 or 3, however, it occupies the slot from performing other arithmetic operations. This means that the operands required for the floating point units arrive at delayed timings and they can't be utilized by the bypass float instructions.

\section{CPU design II}
\subsection{Modifications to instruction}
To handle the problem faced due to writeback operation in the previous design, the new instruction set follows a `split' pipeline approach. The writeback operation is independent of other operations and hence can run in parallel to other operations.

To handle the memory load problem, the memory width is altered to be 4-words(32$\times$4=128 bits) wide. The slots can use the arithmetic units and memory as operands in addition to the registers. For example, \code{addf addf r0} and \code{addf mem r0} are valid instructions. Bypassing in between slots is possible for \code{add}, \code{mulf}, \code{addf} operations. This will be explained in the upcoming subsections.

An additional move instruction \code{movc} is added in this design. It checks for the predicate condition and then moves the value from one of the source registers to the destination register. If no predicate condition is specified, the predicate condition is set to true. The instruction format is \code{if(px) movc rd rs1 rs2}. if the condition is satisfied, the value of \code{rs1} is moved to \code{rd} else, \code{rs2} is moved to \code{rd}. A writeback cannot be issued along with \code{movc} because a destination register is already contained in this instruction. A simplified schematic of the processor is shown in \figurename{ \ref{fig:cpu2plain}}

The resource allocation for the slots is as shown in \tablename{ \ref{table:cpu2slots}}.

\begin{center}
	\begin{table}[h!]
		\centering
		\begin{tabular}{ | c | c | c | c | c |}
			\hline
			Instruction & Slot 0 & Slot 1 & Slot 2 & Slot 3  \\ [0.5ex] 
			\hline\hline
			\code{itof} & - & - & \checkmark & \checkmark \\
			\code{memrd, memwr} & - & - & - & \checkmark \\
			\code{movc} & \checkmark & \checkmark & \checkmark & \checkmark \\
			\code{spike} & - & \checkmark & - & - \\
			\code{wb} & \checkmark & \checkmark & \checkmark & \checkmark \\
			\hline 
		\end{tabular}
		\caption{Modifications to resource allocation. \code{movc} and \code{itof} are added. \code{ld, st} are replaced by \code{memrd, memwr} respectively.}
		\label{table:cpu2slots}
	\end{table}
\end{center}

\begin{figure}
	\centering
	\includegraphics[scale=0.45]{bilder/plainschema.png}
	\caption{Simplified schematic for CPU design II}
	\label{fig:cpu2plain}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[scale=0.45]{bilder/alustart.png}
	\caption{ALU operation initiation. Eg:\code{add r0 r1}}
	\label{fig:aluinit}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[scale=0.45]{bilder/writeback.png}
	\caption{Result writeback. Eg:\code{wb r1 add}}
	\label{fig:resultwb}
\end{figure}

\subsubsection{Split pipeline approach}
All the operations except memory store and conditional move have a separate initiation step and writeback step. Hence, it is sufficient to have the predicate register's value available only when the arithmetic unit has completed its operation or during memory store. Also, having an explicit writeback ensures that there are no collisions in the register write stage, i.e, we can detect these collisions during instruction scheduling when there are two results at the same clock cycle trying to write to the register file through the same register write port. Writeback is not an independent sub-instruction occupying the entire slot, it is now a part of all the sub-instructions and require a fixed encoding space. For example, \code{wb r0 addf addf r3 r4} has two parts, where \code{wb r0 addf} is the result-writeback part and \code{addf r3 r4} is the execute-initiation part. This operation was performed only for floating point oepration in CPU design I, but in this design, all the operations except \code{movc} instruction have to use the writeback instruction to write to the register file. This is shown in \figurename{ \ref{fig:aluinit}} and \figurename{ \ref{fig:resultwb}}.

\subsubsection{Memory operations}

The memory read and write are performed by \code{memrd} and \code{memwr} instructions respectively. This instruction naming is chosen in order to differentiate from the load and store of CPU design I. To precisely control the position in memory to write, an additional 4-bits are added to represent the write-enable bits which are global to all the slots. These bits are appended to the end of the whole bundle. Hence, the total encoding width is  4$\times$38+4=156.  A memory write uses \textbf{first operand} that is available at the instant the \code{memwr} instruction is issued. For example, the bundle
\\
\code{ addi r5 10 || addi r6 15 || nop || memwr r31 r0 3} \\
writes the value in \code{r5} and \code{r6} to the memory location \code{r0}. The value in \code{r31} isn't stored because the \code{memw} bit($ 3_{10}=0011_{2} $) for the slot 3 is zero. The memory mapping for \code{memwr} is shown in \figurename{ 4.5}. The processor schematic for memory write is shown in \figurename{ 4.7}

 Memory read is also linked to the slots in a similar way. An added nuance to this memory handling is that the writeback(\code{wb rd mem}) part of the instruction should be readily available for the utilization if we need to store the value to a register after performing a \code{memrd raddr}(\code{raddr} contains the address). The memory can also directly be used into an operation. For example, \code{mulf mem r0} is a valid instruction. The memory operations can be initiated only by the slot 3 but the data flow is from or to any of the slots. The flow of \code{memrd} is illustrated in \figurename{ 4.6}.
 
 \begin{figure}
 		\centering
 		\begin{tikzpicture}
 		\draw (0,0) rectangle (10,0.5);
 		\draw (2.5,0) -- (2.5,0.5);
 		\draw (5,0) -- (5,0.5);
 		\draw (7.5,0) -- (7.5,0.5);
 		\node at (1.3,0.25) {word 3};
 		\node at (3.75,0.25) {word 2};
 		\node at (6.25,0.25) {word 1};
 		\node at (8.75,0.25) {word 0};
 		
 		
 		\draw (0,1) rectangle (10,1.5);
 		\draw (2.5,1) -- (2.5,1.5);
 		\draw (5,1) -- (5,1.5);
 		\draw (7.5,1) -- (7.5,1.5);
 		
 		\node at (1.3,1.25) {slot 3(op1)};
 		\node at (3.75,1.25) {slot 2(op1)};
 		\node at (6.25,1.25) {slot 1(op1)};
 		\node at (8.75,1.25) {slot 0(op1)};
 		
 		\draw[->] (1.25,1) -- (1.25,0.5);
 		\draw[->] (3.75,1) -- (3.75,0.5);
 		\draw[->] (6.25,1) -- (6.25,0.5);
 		\draw[->] (8.75,1) -- (8.75,0.5);
 		\draw[->] (-1, 0.25) -- (0, 0.25);
 		\node at (-2,0.5) {\code{memwr r0 memw}};
 		\end{tikzpicture}
 	\label{fig:memwrcpu2}
 	\caption{Memory write for CPU design II}
 \end{figure}


\begin{figure}
	 \begin{center}
		\begin{tikzpicture}
		\draw (0,0) rectangle (10,0.5);
		\draw (2.5,0) -- (2.5,0.5);
		\draw (5,0) -- (5,0.5);
		\draw (7.5,0) -- (7.5,0.5);
		\node at (1.3,0.25) {word 3};
		\node at (3.75,0.25) {word 2};
		\node at (6.25,0.25) {word 1};
		\node at (8.75,0.25) {word 0};
		
		
		\draw (0,1) rectangle (10,1.5);
		\draw (2.5,1) -- (2.5,1.5);
		\draw (5,1) -- (5,1.5);
		\draw (7.5,1) -- (7.5,1.5);
		
		\node at (1.3,1.25) {slot 3};
		\node at (3.75,1.25) {slot 2};
		\node at (6.25,1.25) {slot 1};
		\node at (8.75,1.25) {slot 0};
		
		\draw[->] (1.25,0.5) -- (1.25,1);
		\draw[->] (3.75,0.5) -- (3.75,1);
		\draw[->] (6.25,0.5) -- (6.25,1);
		\draw[->] (8.75,0.5) -- (8.75,1);
		\draw[->] (-1, 0.25) -- (0, 0.25);
		\node at (-2,0.5) {\code{memrd r0}};
		\end{tikzpicture}
	\end{center}
	\label{fig:memrd2}
	\caption{Memory read for CPU design II}
\end{figure}
 

Due to the association of each of the slots with the memory alignment, we can have unaligned access to memory by reading the value at any slot location. The initialization of values in memory can be optimized by allocating the memory locations with neuron states such that the slots can directly utilize them in an operation as soon as they are fetched. For example, the \code{itof} units are present in slots 2 and 3, so the integer synaptic weights should be stored in the word position 2 and 3 of the memory so that it can be directly used for arithmetic operation.

\begin{figure}
	\centering
	\includegraphics[scale=0.45]{bilder/memw.png}
	\label{fig:memwrschem}
	\caption{Memory write \code{memwr}}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[scale=0.45]{bilder/arithbypass.png}
	\caption{ALU operation with bypassed operand Eg:\code{addf mulf r1}}
	\label{fig:arithbp}
\end{figure}

\subsubsection{Increased variety of operands}
A much higher degree of bypassing of results is possible in this design. The \code{add, addf, subf, mulf} operations can be bypassed across all the slots. These operations are considered because the critical path of a program is usually caused by the floating point operations. The required arithmetic unit is addressed in a cyclic fashion in the clockwise direction with the position after the suffix \textbf{\_(cyclicposition)}. ``cyclicposition" denotes the cyclic offset from one slot to another when moving in the increasing numerical value. For example, consider that a slot 2's \code{mulf} unit needs to access the result of slot 1's \code{addf}. This can be calculated as (2+3)\%num\_slots=1. The calculated cyclic offset is 3 and hence, the final code is written as \code{mulf addf\_3 rs2}. based on cyclic clockwise distance from slot 2 to slot 1 in a 4 slotted \ac{VLIW} core. Bypassing within the same slot doesn't require any suffix.

The writeback and ALU operations can use only one type of ALU operand. Example: \code{wb r5 addf mulf itof r5} is \textbf{not} a valid sub-instruction because it tries to use both \code{addf} and \code{itof} units in the same instruction as the ALU's multiplexer is designed to provide one output.

An operand to an operation can be immediate, register, ALU or memory as shown below. This is performed by a muliplexer and a selector switch which receives all these values. For an ALU operand, the required select signal should also be issued to the ALU's multiplexer, whose result is available at this operand selector multiplexer for the successive operation. Similar operation is performed for obtaining all operands in different slots. An example of arithmetic unit's result getting bypassed for use in another arithmetic operation is illustrated in \figurename{ \ref{fig:arithbp}}

\subsubsection{Predication changes}

 The encoding for predicated operation remains the same as previous design. Any arithmetic operation's initiation doesn't depend on the predicate register's conditions. A significant difference from the CPU design I which had many instructions which performed a complete pipeline from instruction fetch to register write. For example, in the instruction \code{if(p0) wb r5 addf addf r10 addf}, the \code{wb r5 addf} depends on the value of \code{p0} but the \code{addf r10 addf} operation is initiated unconditionally. This is shown in \figurename{ \ref{fig:predicate2}}. The . This is because \code{wb}, \code{jmp}, \code{spike} and \code{memwr} are the only instructions that can change the stored computation result into a register.
 
 
\begin{figure}
	\centering
	\includegraphics[scale=0.45]{bilder/predicate.png}
	\caption{Effect of predicate condition not satisfying}
	\label{fig:predicate2}
\end{figure}

 
 
 \begin{figure}
 	\begin{center}
 		\textbf{Integer, Float and logical operation}
 		\linebreak
 		\linebreak
 		\begin{bytefield}[endianness=big]{38}
 			\bitheader{0-37} \\
 			\bitbox{4}{Pred} & \bitbox{11}{writeback} & \bitbox{2}{} & \bitbox{2}{s2} &
 			\bitbox{2}{s1} & \bitbox{5}{op2} &  \bitbox{5}{op1}
 			& \bitbox{4}{opcode}
 			& \bitbox{3}{Class}
 		\end{bytefield}
 	\end{center}
 	\begin{center}
 		\textbf{Integer and logical operation with immediate}
 		\linebreak
 		\linebreak
 		\begin{bytefield}[endianness=big]{38}
 			\bitheader{0-37} \\
 			\bitbox{4}{Pred} & \bitbox{11}{writeback} & 
 			\bitbox{2}{s1} & \bitbox{9}{extd\_imm} &  \bitbox{5}{op1}
 			& \bitbox{4}{opcode}
 			& \bitbox{3}{Class}
 		\end{bytefield}
 	\end{center}
 	\begin{center}
 		\textbf{Branch instruction}
 		\linebreak
 		\linebreak
 		\begin{bytefield}[endianness=big]{38}
 			\bitheader{0-37} \\
 			\bitbox{4}{Pred} & \bitbox{11}{writeback} & 
 			\bitbox{6}{} &  \bitbox{10}{target}
 			& \bitbox{4}{opcode}
 			& \bitbox{3}{Class}
 		\end{bytefield}
 	\end{center}
 	\begin{center}
 		\textbf{\code{movc} instruction}
 		\linebreak
 		\linebreak
 		\begin{bytefield}[endianness=big]{38}
 			\bitheader{0-37} \\
 			\bitbox{4}{Pred} & \bitbox{2}{} & \bitbox{5}{rd} &\bitbox{4}{} & \bitbox{2}{} & \bitbox{2}{s2} &
 			\bitbox{2}{s1} & \bitbox{5}{op2} &  \bitbox{5}{op1}
 			& \bitbox{4}{opcode}
 			& \bitbox{3}{Class}
 		\end{bytefield}
 	\end{center}
 	\begin{center}
 		\textbf{\code{spike} instruction}
 		\linebreak
 		\linebreak
 		\begin{bytefield}[endianness=big]{38}
 			\bitheader{0-37} \\
 			\bitbox{4}{Pred} & \bitbox{11}{writeback} &
 			\bitbox{11}{} & \bitbox{5}{rspike} & \bitbox{4}{opcode} & \bitbox{3}{Class}
 		\end{bytefield}
 	\end{center}
 	\caption{Instruction encoding for subword in \ac{CPU} design II}
 	\label{fig:encodingcpu2}
 \end{figure}
 
 
 \subsection{Instruction set}
 Each sub-instruction inside a bundle executes a 38-bit word. An encoding for writeback is essential in all the instructions because it is the only way to write to the registers and is necessary to allow that operation by default. Branching and predicate statements remain the same as in the CPU design I. The encoding scheme for different types of instructions is presented in \figurename{ \ref{fig:encodingcpu2}}. The values $s1$ and $s2$ denote the selector switch for each operand used. The ALU bypassing value is contained within the $writeback$ block. The immediate values are sign-extended to 32 bits.



\subsection{Assembly code}
A simple non-pipelined voltage update loop for a single neuron in CPU design II is implemented as shown in Listing { \ref{lst:cpu2example}}. The register \code{r0} initially contains zero.

\lstset{language=Python, backgroundcolor=\color{gray!10}, frame=single} % Set the language of the code block
\lstset{basicstyle=\ttfamily\footnotesize,breaklines=true} % Set the font and allow line breaks
\lstset{numbers=left, numberstyle=\tiny, stepnumber=1, numbersep=5pt, captionpos=b} % Add line numbers
\begin{lstlisting}[caption={Voltage update code block}, label=lst:cpu2example]
MAIN: ### Loading the parameters of neurons
nop || nop || addi r0 1 || memrd r0
# load num_neurons, P22, P21exc , P21inh 
wb r2 mem nop || wb r3 mem nop || wb r4 mem addi add 1 || wb r5 mem memrd add_3
# load P20, P11exc, P11inh, V_th
wb r6 mem nop || wb r7 mem nop || wb r8 mem addi add 1 || wb r9 mem memrd add_3
# load V_reset, r_ref, w_frac_exc, w_frac_inh
wb r10 mem nop || wb r11 mem nop || wb r12 mem addi add 1 || wb r13 mem memrd add_3
#setting the base address of first neuron in r1
nop || nop || nop || wb r1 add

ITER:
#r23 stores base address to write result later
if(p0) jmp EXIT || addi r1 0 || addi r1 1 || memrd r1
#init Vm*P22; isyn_ex*P21ex; isyn_in*P21in; P20*Ie 
mulf mem r8 || wb r23 add mulf mem r10 || wb r1 add mulf mem r11 || mulf mem r13
nop || nop || nop || memrd r1
#compare tref with 0
nop || wb r12 mem cmpeq r31 mem || nop || nop
nop || nop || nop || nop
wb r19 mulf subi r12 1  || wb r20 mulf nop || wb r21 mulf nop || wb r22 mulf nop
#refractory time? decrement t_ref in r12
if(!p1) wb r12 subi addf r19 r20 || addf r21 r22 || nop || nop
#write decremented t_ref to memory
nop || addi r12 0 || nop || memwr r31 r1 2
nop || nop || nop || nop
nop || nop || nop || nop
wb r19 addf nop || wb r20 addf nop || nop || nop
addf r19 r20 || nop || nop || nop
nop || nop || nop || nop
nop || nop || nop || nop
nop || nop || nop || nop
#compare Vth >=Vm
if(p1) wb 19 addf cmpfge r5 addf || nop || nop || nop
nop || nop || nop || addi r1 2
#threshold condition, emit spike and update voltage to V_reset or
#r17 contains final neuron's end address
cmpge r1 r17 || if(!p0) spike r1 || if(!p0) movc r19 r19 r10 || wb r1 add nop
#Updating Vm
addi r19 0 || nop || nop || memwr r31 r23 1
if(!p0) jmp ITER || nop || nop || nop
EXIT: jmp EXIT || nop || nop || nop
\end{lstlisting}
In lines 1 to 10, we initialize the neuron's parameters and initiate the memory pointer in register r1. The register \code{r23} is used to hold the pointer such that the required value is stored in the same memory location for updating $V_{m}$. the predicate values are used for emitting spike, performing jumps and neuron state update.

One iteration of voltage update takes 20 cycles which is an improvement compared to the previous design. The floating point operation's results can be directly forwarded for use in other operations across different slots, this avoids intermediate register storage. Still, there are a lot of cycles with no operation performed. An increased utilization is possible after performing the software pipelining algorithm on this base hardware. 