---
title: "Workfolder Structure"
author: Johnson Loh
date: 2020-12-14
keywords:
crossreference:
bibliography:
csl:
---

## masterthesis_latex_template

# General

The Markdown syntax with yaml parsable headers is used as a style convention due to its flexibility with regard to file conversion and formatting. When adhered to the style, protocols, slides, reports and possibly publication templates can be generated using pandoc:
- pandoc -S -o <outfile>.pdf --filter pandoc-citeproc <infile>.md
The desired output format (cls), citation style (csl), bibliography etc. needs to be specified in the header accordingly.

# Example file types

- Tabulatory data: \*.csv

<!--
# References
-->
