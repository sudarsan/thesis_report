\chapter{Design decisions and challenges}
Traditionally, computers were considered to be sequential machines. Parallel computing uses multiple processing elements simultaneously to solve a problem. The earliest parallel computing was performed using symmetric multiprocessors, where multiple processors share a common memory. In parallel computing, a computational task is broken down to several subtasks that can be processed independently and the corresponding results are combined later. In a modern processing system, communication and synchronization between the subtasks are the main obstacles to optimal performance. Applications are classified based on the level of parallelism within its independent subtasks. If the subtasks need to communicate regularly, it is called fine-grained parallelism. If it is less frequent, it is called coarse-grained parallelism.
  
Pipelining allows a single-issue processor to execute one instruction per clock cycle (\ac{IPC} = 1) by running different stages of an operation for different instructions simultaneously. The key concept of a RISC machine is that each instruction performs only one function. This feature of a \ac{RISC} CPU enables predictable instruction times and, hence, it simplifies the design of the system. Contrary to this approach, the \ac{CISC} architecture can execute several operations in a single instruction. Instructions were highly encoded so as to increase code density. This facilitated a smaller code size and lesser memory accesses. Modern x86 processors decode and split complex instructions into dynamic sequences of micro-operations. These instructions can be re-ordered so that the dependencies are avoided. The combined benefits of code density of CISC and shorter pipeline of a RISC processor results in a better performance. Most modern processors have multiple execution-units which can execute multiple instructions in parallel.

\textit{Systolic array} is a homogeneous network of data processing units called nodes. Each node independently computes a partial result as a function of the data received and passes it downstream. Classification of systolic arrays is controversial because sometimes they are classified as \ac{MISD} and they can  also considered to be \ac{MIMD}. This is due to the nature of the input data which can be viewed either as a single vector input or as set of inputs  whose information must be combined and operated in order to get the desired result out of a systolic array. \cite{1653825}

  As an initial step, we explore the different types of computing architectures and then select an approach which is a reasonable fit for our requirement.

\section{Processor type}
For specialized parallel workloads, systolic arrays are faster than general purpose processors due to the absence of memory accesses during execution stages which prevents a huge overhead in time and memory \cite{1653825}. These arrays are very successful in the areas of artificial intelligence and image processing\cite{jouppi2017datacenter}. However, they are not compatible to solutions where highly unpredictable flow of instructions is required. The diversity of available neuron models and their complicated computational requirement is a constraint which eliminates systolic arrays as a possible design choice.

Considering the diversity of the neuron models, we need to adopt a design that encourages a high degree of flexibility. Our design aims to simulate a large number of neurons. This limits our search to look into \ac{VLIW}(static multiple-issue) and superscalar(dynamic multiple-issue) processors\cite{shen2013modern}. VLIW processors fetch and execute multiple instructions in the same order as it is available in the program memory whereas a superscalar processor dynamically selects the instructions to be executed in parallel.

Superscalar processors are a possible fit for our application since they support parallel execution of independent instructions. The target neuron simulation contains a fixed program for a predefined neuron and synapse model. This means that the programmer has the potential to handle the dependencies with more control. This requires a greater optimization effort towards the instruction scheduler and compiling aspect of the implementation. Static scheduling of instructions can greatly reduce the necessity for dynamic instruction issue. Furthermore, implementing the aspects of dynamic scheduling significantly increases hardware cost, which can be conveniently avoided when instruction scheduling can be done beforehand.

Based on the above shortcomings of a superscalar processor, a reasonable choice of design between the two is a \ac{VLIW} processor since they can execute multiple instructions in parallel while avoiding the bulky dynamic scheduling hardware. The ability to perform independent computations of multiple neurons(for a time-driven simulation) in a processing unit characterizes our application to exhibit fine-grained parallelism. Having dedicated longer pipelines as in \ac{CISC} for special instructions introduces complex hardware which might not be reused frequently. Due to the presence of a simpler instruction set and lesser hardware complexity, we adopt a \ac{RISC}-style instruction set. Having more frequent memory accesses is a downside of adopting a RISC processor. This drawback can be possibly avoided by using a better memory access mechanism. Considering the limited availability of \acp{CLB} within the \ac{FPGA}, a \ac{RISC} processor would fit in with lesser resources such that the remaining parts of the neuromorphic system is able to easily adopt our neuron processor.

Biologically inspired models require a higher dynamic range compared to integrate and fire models. Floating-point numbers are opted for this reason and there are various precision widths that can be used. Choosing a very high dynamic range or precision would account for very high computation times and require a large amount of memory. The IEEE 754 single-precision floating point value is chosen for the computation of neuron states as a balance between achieving a suitable dynamic range along with the relatively lesser hardware and memory it requires. 
To handle neuronal computations where a high dynamic range or precision is not required, the design should be capable of flexible data width and memory units. This is one of the primary advantages of an \ac{FPGA}-based implementation.

\section{Memory organization}
The general purpose processor has to operate on all the different neuron's states along with the external inputs(incoming spikes) to compute the state for next timestep. Since shared registers are not sufficient to hold the states of multiple neurons, a \ac{BRAM} which is built into the \ac{FPGA} fabric is used. This design uses a Harvard architecture as there is a higher performance offered by having a separate memory. In our simulations, the target program will not be altered by any process within the processing core. Also, having a separate memory for program and data allows for an easier modification of data width and avoids memory wastage by adjusting the data memory width independent of the instruction requirements. This also enables flexibility in the number of parallel units within a \ac{VLIW} word if there is a possibility of exploiting more parallelism by static instruction scheduling.



\section{Instruction Dependencies}
Data dependencies are an inherent part of any instruction-based processing system. A program cannot run quicker than the longest chain of dependent operations called as the critical path. An example of critical path is shown in \figurename{ \ref{fig:dfgiaf}} and the dependency graph of membrane potential update is show in \figurename{ \ref{fig:dependency}}. When performing an operation which requires multiple cycles(say, floating point addition), any operation which uses the result of this computation needs to wait. The latency cycles caused by these operations should ideally be occupied by other independent useful instructions. Search for a good scheduling algorithm to order these instructions is required to ensure maximum utilization of the underlying hardware. The primary goal is to keep the processor as busy as possible with useful computation.


 It is worth noting that the number of registers available to store neuron  parameters limits our ability to operate across multiple neurons, which may become a constraint when dealing with increasingly complex functions. Performing register allocation and spilling in a parallel processor is one of the major challenges. The entire computation can be performed either by parallel execution of different parts of a single neuron or by executing the same operation across multiple neurons. Depending on the amount of dependencies and possibility of parallelism within a single neuron computation, a choice must be made whether or not to simulate multiple neurons simultaneously.

\begin{center}
	\begin{figure}[!ht]
		\includegraphics[scale=0.8]{bilder/neuron_dfg.jpeg}
		\centering
		\caption{Schematic of membrane potential V$_{m}$ update for exponential \ac{IAF} model with \ac{CUBA} synapse. The critical path of V$_{m}$ is highlighted.}
		\label{fig:dfgiaf}
	\end{figure}
\end{center}

\begin{figure}
	\centering
	\begin{tikzpicture}[node distance=1cm]
		\node (Vm) at (0,0) {$V_{m}$(t)};
		\node (P22) at (2,0) {$P_{22}$};
		\node (Isynin) at (3,0) {$I_{syn,in}$};
		\node (P21inh) at (4.7,0) {$P_{21,in}$};
		\node (Isynex) at (6,0) {$I_{syn,ex}$};
		\node (P21exc) at (8,0) {$P_{21,ex}$};
		\node (Iext) at (9,0) {$I_{ext}$};
		\node (P20) at (11,0) {$P_{20}$};
		\node (VmP22) [circle, draw] at (1,-1) {*};
		\node (IsynP21inh) [circle, draw] at (4,-1) {*};
		\node (IsynP21exc) [circle, draw] at (7,-1) {*};
		\node (IextP20) [circle, draw] at (10,-1) {*};
		\node (s1) [circle, draw] at (2.5,-2) {+};
		\node (s2) [circle, draw] at (8.5,-2) {+};
		\node (s3) [circle, draw] at (5.5,-3) {+};
		\node (s4) at (5.5 , -4) {$V_m$(t+1)};
		
		\draw [->] (Vm) -- (VmP22);
		\draw [->] (P22) -- (VmP22);
		\draw [->] (Isynin) -- (IsynP21inh);
		\draw [->] (P21inh) -- (IsynP21inh);
		\draw [->] (Isynex) -- (IsynP21exc);
		\draw [->] (P21exc) -- (IsynP21exc);
		\draw [->] (P20) -- (IextP20);
		\draw [->] (Iext) -- (IextP20);
		\draw [->] (IsynP21inh) -- (s1);
		\draw [->] (VmP22) -- (s1);
		\draw [->] (IsynP21exc) -- (s2);
		\draw [->] (IextP20) -- (s2);
		\draw [->] (s1) -- (s3);
		\draw [->] (s2) -- (s3);
		\draw [->] (s3) -- (s4);
	\end{tikzpicture}
	\caption{Dependency graph for a sub-step in updating $V_m$ of exponential \ac{IAF} model with \ac{CUBA} synapse.}
	\label{fig:dependency}
\end{figure}
 
% Another example highlighting the problem of dependency is a simple numerical solver like the fourth-order Runge Kutta(RK4)\cite{süli_mayers_2003} method shown in \figurename{ \ref{fig:rk4}}. There are long distance dependencies during the run-time of such numerical solvers. The function $f$ in the figure represents a set of ordinary differential equations. For example, an exponential integrate-and-fire neuron model\cite{meffin_burkitt_grayden_2004} with \ac{COBA} synapse is defined by
% 
% 
% \begin{center}
% 	
% 	$\frac{dV}{dt} = \frac{( -I_L + I_{ext} + I_{syn,exc} - I_{syn,inh})}{C_m}$ \\
% 	
% 	$\frac{dg_{exc}}{dt} = \frac{g_{exc}}{\tau_{syn,exc}}$\\
% 	
% 	$\frac{dg_{inh}}{dt} = \frac{-g_{inh}}{\tau_{syn,inh}}$\\
% \end{center}
% 
% The values $g_{exc}$ and $g_{inh}$ for successive timesteps are computed numerically and they are in turn used to obtain synaptic currents. Such complex functions are cases where the number of registers might become a constraint from executing multiple neurons in parallel.
%
%\begin{center}
%	\begin{figure}[!ht]
%		\centering
%		\begin{tikzpicture}[node distance=1.5cm]
%			\node (begin) [align=center] {$y'=f(t,y)$; \\ $y(t_0)=w_0$; \\ $h=const.$};
%			\node (stage1) [draw, below of=begin, align=center, minimum width=5cm] {$k_1=hf(t_0,w_0)$};			\node (stage2) [draw, below of=stage1] {$k_2=hf(t_0+ \frac{h}{2}, w_0+\frac{k_1}{2})$};
%			\node (stage3) [draw, below of=stage2, align=center, minimum width=5cm] {$k_3=hf(t_0+\frac{h}{2}, w_0+\frac{k_1}{2})$};
%			\node (stage4) [draw, below of=stage3, align=center,  minimum width=5cm] {$k_4=hf(t_0+h, w_0+k_3)$};
%			\node (stage5) [draw, below of=stage4, align=center,  minimum width=5cm] {$y(t_0+h)=w_0+\frac{(k_1+2k_2+2k_3+k_4)}{6}$};
%			
%			\draw [->] (begin) -- (stage1);
%			\draw [->] (stage1) -- (stage2);
%			\draw [->] (stage2) -- (stage3);
%			\draw [->] (stage3) -- (stage4);
%			\draw [->] (stage4) -- (stage5);
%		\end{tikzpicture}
%		
%		\caption{High-level data flow graph of RK4 numerical integration. Note the dependency caused by the slopes \textbf{$k_i$}.}
%		\label{fig:rk4}
%	\end{figure}
%\end{center}

\section{Branches}
\textit{Trace scheduling}\cite{1675827} is a strategy to handle branches in  \ac{VLIW} instructions. This approach selects the most frequently executed paths and builds traces. This long sequence of instructions might be packed closer together to increase the code density and reduce the total number of cycles. Traces are determined by code profiling and predicting the most probable paths. The required compensation or fix-up code must be provided such that the semantic correctness of the code remains unchanged if the path taken is different from the trace. This method avoids branches but can be used only in cases where acyclic flow of data is available.

In the absence of dependencies, parallel operations are possible. Encountering branches during such parallel operations is a problem. This means that when a branch operation is executed, the subsequent operations remain uncertain. The instructions staged for the next cycle may or may not be executed and hence, the parallel computations which are independent of the branch also can't be scheduled. This is a huge wastage of computing resources. A common approach to handle branches in the VLIW approach is to use \textit{predicated execution}\cite{mahlke1995comparison}. In this approach, the condition result is stored in \textit{predicate registers}. The conditional branch statements can be replaced by predicated execution.  This is illustrated in \figurename{ \ref{fig:predicate}}, the program flow contains conditional execution of both operation A and B. The removal of control dependencies enable more scope for instruction scheduling optimizations. Primary advantage of predication is a better utilization of hardware which is by mixing the conditional and unconditional code to run in parallel.


\begin{figure}
	\centering
	\begin{minipage}[b]{0.45\linewidth}
		\centering
		\begin{tikzpicture}[node distance=2cm]
			\node (start) {Start};
			\node (decision) [diamond, aspect=1.5, draw, below of=start] {cond};
			\node (process2a) [draw, below left of=decision] {A};
			\node (process2b) [draw, below right of=decision] {B};
			\node (output) [draw, below right of=process2a] {C};
			\node (end) [below of=output] {End};
			
			\draw [|->] (start) -- (decision);
			\draw [->] (decision) -| node [near start, above] {Yes} (process2a);
			\draw [->] (decision) -| node [near start, above] {No} (process2b);
			\draw [->] (process2a) |- (output);
			\draw [->] (process2b) |- (output);
			\draw [-|] (output) -- (end);
		\end{tikzpicture} 
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.45\linewidth}
		\centering
		\begin{tikzpicture}[node distance=1.33cm]
			\node (start) {Start};
			\node (decision) [draw, below of=start, fill=blue!10] {p=(cond)};
			\node (process2a) [draw, below of=decision] { \textit{if(p)} $\rightarrow$ A };
			\node (process2b) [draw, below of=process2a] {\textit{if(!(p))} $\rightarrow$ B};
			\node (output) [draw, below of=process2b] {C};
			\node (end) [below of=output] {End};
			
			\draw [|->] (start) -- (decision);
			\draw [->] (decision) -- (process2a);
			\draw [->] (process2a) -- (process2b);
			\draw [->] (process2b) -- (output);
			\draw [-|] (output) -- (end);
		\end{tikzpicture}	
	\end{minipage}
	\caption{The branched flow is shown to the left and a predicated flow is shown to the right. Condition storage in register p is highlighted.}
	\label{fig:predicate}
\end{figure}

\section{Bypassing}
Integrating the floating-point operations within the ALU introduces long latency to obtain the results. If the destination register and the operation are specified along with the instruction, there needs to be a mechanism to hold a list of destination registers and write signals throughout the pipeline of such floating point operation. This introduces a lot of additional hardware which is in contrast to our requirement of having a smaller design. This is avoided by using a special \textbf{writeback} instruction. In this approach, the initiation of an operation only supplies the operands and doesn't specify the destination register. Later, the writeback instruction is explicitly issued to directly obtain the computed result. This is one of the significant design decisions.

In a pipelined processor, data dependencies among successive operations are detected and handled by comparison of destination register indices. The number of comparators required for bypassing within a \ac{VLIW} processor increases greatly with increase in parallel units. For a processor with $n$ functional units and $d$ bypass source stages, the number of required comparators is $2dn^2$ \cite{285612}. The multiplexer within the ALU  combined with the bypassing logic infers a very large combinational logic. As a measure to meet the timing requirements and also to enable easier future modifications, forwarding the results across multiple execution pathways is not supported. The absence of pipeline stall means that there needs to be explicit NOPs in the code. The trade-off here is to have fewer hardware logic while having an increased usage of instruction memory.
