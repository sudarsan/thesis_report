\chapter{Results and measurements}
An exact software pipelining algorithm is not available for our current hardware. A methodology which is inspired by Modulo Variable Expansion\cite{10.1145/960116.54022} where the loop is unrolled several times and variables are renamed. The resulting performance is measured and compared with other designs like RISC-V and dedicated \ac{HLS} model for the simulation of a specific neuron model.

\ac{HLS}, also called as electronic system-level synthesis is a process of converting the behavioral specification of a hardware in a high level language to the \acs{RTL} structure which realizes the given behavior. In our setup, we use the Xilinx Vitis HLS tool to convert the C++ version of our model to generate the required behavioral \ac{RTL} model.

The presence of a different memory access and allocation pattern means that we have to adopt a methodology which doesn't completely match the requirement of already available software pipelining strageties. Hence, a heuristic is applied for a particular initialization configuration of the \ac{BRAM}.

\section{Software pipelined code generation}
The most important step in this process is to find a minimum \ac{II}\,(the interval between two successive iterations). Having a low \ac{II} means that the throughput of the generated code is high.
 Many possible combinations of memory allocation is possible in the search for a better throughput. 

\begin{enumerate}
	\item Set a minimum value of II.
	\item Schedule the simulation code for the first iteration.
	\item Perform a loop unroll after II cycles.
	\item If there are resource collisions, increase the II and go to step 3. Else go to step 4.
	\item If there are no collisions, continue unrolling next iterations. Move to the step 6 when it is no longer possible to unroll.
	\item If it is possible to schedule the latest iteration by jumping back to the previous cycles, proceed to step 7. Else, reschedule the code by altering the memory allocation and go to step 1.
	\item Add the necessary epilogue code to finalize the unfinished iterations.
\end{enumerate}


\subsection{Assembly code}
The obtained kernel for the voltage loop and current loop with their memory maps is as shown in Listing \ref{lst:pipelinevolt} and Listing \ref{lst:pipelinecurrent} respectively. It has to be noted that the critical path of any iteration is not minimized and only the interval between each iteration or the throughput can be maximized as long as the hardware resources are available.

The iteration of the latest loop starts by \code{memrd r0} and the register \code{r0} and \code{r1} contains the two rows of neuron states' address. The values from memory are directly sent to perform multiplication. Then, we read $t_{ref}$ from memory and store it in \code{r21}. The four multiplication values are stored back into registers \code{r14, r15, r16} and \code{r17}. Meanwhile, the refractory condition is checked for zero with \code{r31} containing zero. If the condition is satisfied, update the value of $t_{ref}$ in memory. Now, the registers \code{r14} to \code{r17} are used again after looping back to line 1. The values are added and their results are written back to registers \code{r18} and \code{r19}. These these two values are again added and the result is stored in \code{r20} based on the refractory condition. The threshold condition is checked and the updated value is written to the memory.


\begin{center}
	\begin{bytefield}[bitwidth=0.25em]{128}
		\bitheader{0,31,63,95,127} \\
		\bitbox{32}{r2= num neur*2} & \bitbox{32}{r3= $P_{22}$} &
		\bitbox{32}{r4= $P_{21,exc}$} & \bitbox{32}{r5= $P_{21,inh}$} \\
		\bitbox{32}{r6= $P_{20}$} & \bitbox{32}{r7= $P_{11,exc}$} &
		\bitbox{32}{r8= $P_{11,inh}$} & \bitbox{32}{r9= $V_{th}$} \\
		\bitbox{32}{r10= $V_{reset}$} & \bitbox{32}{r11= $t_{ref,rst}$} &
		\bitbox{32}{r12= $w_{frac,exc}$} & \bitbox{32}{r13= $w_{frac,inh}$} \\
		\bitbox[tlr]{32}{$V_{m}$} & \bitbox[tlr]{32}{$I_{syn,exc}$} &
		\bitbox[tlr]{32}{$I_{syn,inh}$} & \bitbox[tlr]{32}{$I_{ext}$} \\
		\bitbox{32}{$w_{exc}$} & \bitbox{32}{$w_{inh}$} &
		\bitbox{32}{} & \bitbox{32}{$t_{ref}$} \\
		\bitbox[tlr]{32}{$V_{m}$(2)} & \bitbox[tlr]{32}{$I_{syn,exc}(2)$} &
		\bitbox[tlr]{32}{$I_{syn,inh}(2)$} & \bitbox[tlr]{32}{$I_{ext}(2)$} \\
		\bitbox{32}{$w_{exc}(2)$} & \bitbox{32}{$w_{inh}(2)$} &
		\bitbox{32}{} & \bitbox{32}{$t_{ref}(2)$} \\
	\end{bytefield}
\end{center}

\lstset{language=Python, backgroundcolor=\color{gray!10}, frame=single} % Set the language of the code block
\lstset{basicstyle=\ttfamily\footnotesize,breaklines=true} % Set the font and allow line breaks
\lstset{escapeinside={<@}{@>}}
\lstset{captionpos=b, numbers=none} % Add line numbers
\begin{lstlisting}[caption={Memory map and kernel code for $V_m$ and $t_{ref}$ update. Code in black, red and blue represents successive iterations. Latency=24, throughput=$f$/8}, label=lst:pipelinevolt]
LOOP: <@\textcolor{red}{addf r14 r15}@> || <@\textcolor{blue}{subi r2 1}@>  || <@\textcolor{red}{addf r16 r17}@> || <@\textcolor{blue}{memrd r0}@>

wb r20 addf <@\textcolor{blue}{mulf mem r3}@> || <@\textcolor{blue}{wb r2 sub mulf mem r4}@> ||
            <@\textcolor{blue}{mulf mem r5}@> || <@\textcolor{blue}{mulf mem r6}@>

<@\textcolor{blue}{addi r0 2}@> || cmpflt r9 r20 || <@\textcolor{blue}{subi r0 4}@> || <@\textcolor{blue}{memrd r1}@> 

<@\textcolor{blue}{wb r0 add}@> || nop || <@\textcolor{blue}{ wb r22 sub cmpeq r2 r31}@> ||<@\textcolor{blue}{wb r21 mem}@>

<@\textcolor{red}{wb r18 addf}@> <@\textcolor{blue}{cmpeq r21 r31}@> || if(p1) spike r2 ||
          <@\textcolor{red}{wb r19 addf}@> || if(p1) movc r20 r10 r20

<@\textcolor{blue}{wb r14 mulf}@> <@\textcolor{red}{addf r18 r19}@> || <@\textcolor{blue}{wb r15 mulf}@> ||
          <@\textcolor{blue}{wb r16 mulf addi r1 2}@> || <@\textcolor{blue}{wb r17 mulf subi r21 1}@> 

addi r20 0|| <@\textcolor{blue}{if(!p0) wb r21 sub\_2}@> ||<@\textcolor{blue}{wb r1 add}@> || if(p1) memwr r31 r22 1

<@\textcolor{blue}{if(p2) jmp LOOP}@> || nop || nop || <@\textcolor{blue}{if(!p0) memwr r21 r1 8}@>
\end{lstlisting}


\begin{center}
	\begin{bytefield}[bitwidth=0.25em]{128}
		\bitheader{0,31,63,95,127} \\
		\bitbox{32}{r2= num neur*2} & \bitbox{32}{r3= $P_{22}$} &
		\bitbox{32}{r4= $P_{21,exc}$} & \bitbox{32}{r5= $P_{21,inh}$} \\
		\bitbox{32}{r6= $P_{20}$} & \bitbox{32}{r7= $P_{11,exc}$} &
		\bitbox{32}{r8= $P_{11,inh}$} & \bitbox{32}{r9= $V_{th}$} \\
		\bitbox{32}{r10= $V_{reset}$} & \bitbox{32}{r11= $t_{ref,rst}$} &
		\bitbox{32}{r12= $w_{frac,exc}$} & \bitbox{32}{r13= $w_{frac,inh}$} \\
		\bitbox[tlr]{32}{$V_{m}$} & \bitbox[tlr]{32}{$I_{ext}$} &
		\bitbox[tlr]{32}{} & \bitbox[tlr]{32}{$t_{ref}$} \\
		\bitbox{32}{$I_{syn,exc}$} & \bitbox{32}{$I_{syn,inh}$} &
		\bitbox{32}{$w_{exc}$} & \bitbox{32}{$w_{inh}$} \\
		\bitbox[tlr]{32}{$V_{m}$(2)} & \bitbox[tlr]{32}{$I_{ext}$(2)} &
		\bitbox[tlr]{32}{} & \bitbox[tlr]{32}{$t_{ref}$(2)} \\
		\bitbox{32}{$I_{syn,exc}$(2)} & \bitbox{32}{$I_{syn,inh}$(2)} &
		\bitbox{32}{$w_{exc}$(2)} & \bitbox{32}{$w_{inh}$(2)} \\
	\end{bytefield}
\end{center}


\lstset{language=Python, backgroundcolor=\color{gray!10}, frame=single} % Set the language of the code block
\lstset{basicstyle=\ttfamily\footnotesize,breaklines=true} % Set the font and allow line breaks
\lstset{escapeinside={<@}{@>}}
\lstset{numbers=left, numberstyle=\tiny, stepnumber=1, numbersep=5pt, captionpos=b} % Add line numbers
\begin{lstlisting}[caption={Memory map and kernel code for updating currents, latency=10, throughput=$f$/5}, label=lst:pipelinecurrent]
LOOP: <@\textcolor{black}{addf mulf r19}@> || <@\textcolor{black}{addf mulf r20}@> || <@\textcolor{blue}{addi r0 2}@> || <@\textcolor{blue}{memrd r0}@>
<@\textcolor{blue}{mulf r7 mem}@> || <@\textcolor{blue}{mulf r8 mem}@> ||  <@\textcolor{blue}{wb r0 add itof mem r12}@> || <@\textcolor{blue}{itof mem r13}@>
<@\textcolor{blue}{cmpeq r0 r2}@> || nop || nop || nop
nop || nop || <@\textcolor{blue}{wb r19 itof}@> || <@\textcolor{blue}{wb r20 itof}@> addi r0 -4
<@\textcolor{black}{addi addf 0}@> || addi addf 0 || <@\textcolor{blue}{if(!p0) jmp LOOP}@> || memwr r31 addi 3
\end{lstlisting}

\section{RISC-V implementation}
A C program containing the voltage loop over four neurons over multiple timesteps is taken as a benchmark. To simulate a RISC-V core, the Chipyard project \cite{9099108} is used to build the required System-on-Chip. This project generates the required RTL from the high-level SoC specification written in Chisel hardware construction language which is an embedded language based on Scala. This process is performed by the Chisel hardware compiler \cite{6241660}. The riscv-tools is an open-source project that contains a collection of RISC-V ISA simulator, bootloader and the tests. Our required neuron simulation can be performed by compiling with the compatible compiler for the generated hardware. Verilator is a simulator used to perform cycle-accurate simulation of a test program on the RTL-based RISC-V SoC generated by Chisel. The throughput and latency of the simulation can be obtained by viewing the generated waveform file using the open-source tool GTKWave.

The Chipyard project supports the following processor cores: 1) Rocket 2) \ac{BOOM} 3) CVA6 and 4) Ibex. Except BOOM, other cores are of in-order processing type. Our benchmark is performed on Rocket core and BOOM due to the presence of a better support and test environment. Rocket is a 5 stage in-order scalar processor while \ac{BOOM} is a 10 stage out-of-order superscalar processor with 4-wide decode. To obtain the resource utilization of this processor, the memory elements and interfaces contained in the generated RTL for simulation are stripped down and then synthesized using Vivado for the same target FPGA as used for the previous CPU implementations.


The Rocket core is tested using the steps specified in the (\url{https://github.com/chipsalliance/rocket-chip}) repository which is a sub-project of Chipyard. The default setup of this project generates a Rocket core based SoC. This hardware supports integer multiplication(M), Atomic instructions(A) and single precision Floating point(F) with the base 64-bit RISC-V architecture. The target C code is written within the riscv-tests repository under benchmarks. The file ``rocket-chip/src/main/scala/system/RocketTestSuite.scala" is edited to include our added files as a valid benchmark. The C code is made compatible with the benchmarks folder of the riscv-tests repository which is a part of riscv-tools project. The compiler optimizations are all set to the default configuration of the riscv-tests. The executable is generated by simply running the benchmarks initially using the available scripts contained in the rocket-chip project. 

The generated executable is then run by Verilator object which is built to simulate the hardware. Verilator is run with the configuration ``DefaultConfig" in the rocket-chip repository for Rocket core simulation and it is run with ``LargeBoomConfig" configuration in the Chipyard repository (\url{https://github.com/ucb-bar/chipyard}) for the simulation of BOOM core. The simulation process for BOOM is the same as Rocket core as both are run by Verilator, only the generation of the SoC  utilizes a different configuration. The riscv-gnu-toolchain contains a list of compilers and spike-disassembler(spike-dasm). spike-dasm can be used to view the clock cycle, program counter and the instruction being executed. Alternatively, Verilator can also be configured to output the wave diagram.

\section{Performance and hardware utilization(Voltage loop)}
The performance is to be measured in terms of latency and throughput for designs that satisfy the target frequency. The designs chosen for comparison are HLS model, CPU design I, CPU design II, RISC-V with RocketChip core and RISC-V with BOOM core. For our CPU design II, the performance improvement due to the loop unrolling and software pipelining are also analyzed. For a fair comparison, all these models only perform the voltage update equation. It should provide an approximate estimate of the performance due to the presence of branches, dependencies in float operations and state update to the memory.

\begin{figure}
	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{bilder/performance.png}
	\caption{Latency and Throughput of implementations}
	\label{fig:performance}
\end{figure}

The latency and throughput of these designs is shown in \figurename{ \ref{fig:performance}}.

The difference in latency between design I and design II is due to an instruction set with better forwarding and faster memory access. When a 2x loop unrolling is performed on the CPU design II, the register file with 32 registers was sufficient as we required only one additional pointer variable for the load/store operations. This was done. The latency remains the same for software pipelining as we performed a 3x unroll with an \ac{II} of 8 cycles initially and then applied the heuristic, a kernel of width 8 was obtained which produces 8$\times$3=24 cycles to finish its execution. Further unrolling of this code might be possible with an increased latency, hence, this value is one of the possible metric for loop unrolling or software pipelining in general. Rocket core has the highest latency due to its in-order processing and a delay due to instruction fetch and stalling. The \ac{BOOM} core has a reduced latency compared to Rocket due to its superscalar capability. The dedicated HLS version has a longer latency compared to our designs which might be because it is more optimized to achieve a higher throughput.

\begin{table}[ht]
	\centering
	\begin{tabular}{|c|>{\raggedleft}p{2cm}|>{\raggedleft}p{2cm}|c|}
		\hline
		\multicolumn{1}{|c|}{\centering Core} & \multicolumn{1}{c|}{LUT} & \multicolumn{1}{c|}{FF} & \multicolumn{1}{c|}{DSP} \\
		\hline
		CPU design I & 13,622 & 12,919 & 16 \\
		CPU design II & 9,763 & 5,362 & 16 \\
		Dedicated HLS & 1,871 & 4,583 & 18\\
		Rocket & 15,109 & 5,953 & 14 \\
		BOOM & 224,000 & 77,800 & 39\\
		\hline
	\end{tabular}
	\caption{Hardware utilization of implementations}
	\label{tab:resourceutil}
\end{table}

The CPU design I, CPU design II, HLS and Rocket core were able to run at 200 MHz and the BOOM core could be implemented only at 88 MHz. This is reasonable due \ac{BOOM}'s higher design complexity. The throughput calculation follows from the latency and operating frequency for execution without unrolling. For execution of loops without unrolling, the throughput is obtained by simply dividing the operating frequency by the latency. For a 2x unrolled loop, it is calculated in a similar way but finally multiplied by a factor of 2. The value is calculated as follows, the latency of each neuron is increased to 28 due to unrolling step at the 8th cycle. Hence, the throughput is given as 200/28 $\times$ 2 = 14.28. During the unrolling process, the registers and hardware resources were sufficient to hold the states of two neurons. Software pipelined version of CPU design II has a higher throughput as it is able to generate an output every 8 cycles. The highly specialized HLS version is able to generate an output in each cycle, providing the highest throughput. Rocket has a low throughput due to its in-order and sequential execution. Though \ac{BOOM} is able to have lower latency compared to Rocket, it is limited by its throughput due to its maximum operating frequency. Hence, its latency is calculated by its maximum frequency divided by latency which is 88/42=2.1\,MHz.


The hardware utilization in terms of \ac{LUT}, \ac{FF} and \acs{DSP}
is shown \tablename{ \ref{tab:resourceutil}}. The huge difference in \ac{FF} consumption between CPU design I and design II is due to the greater presence of registers along the full data path in pipeline in design I which are simply handled by instructions and appropriately added multiplexers in design II. Within our CPU implementations, the Register file is the highest consumer of LUTs\,(4618 and 4542 for design I and design II respectively). This might be due to the presence of a very high fan-out. The \ac{BOOM} core has the highest utilization due to its instruction issue, renaming and branch prediction logic. In general, the low performance of the RISC-V cores can be attributed to the reason that they have to support general purpose computing and hence have a lot of additional logic for multi-level memory handling.


The \ac{IPC} for design I and design II for a simple loop without unrolling is 1.34 and 1.56 respectively which is much lower that the maximum attainable value of 4. The design II is able perform at 3.125 instructions per cycle for the software pipelined version. For this calculation, only the kernel part of the code is considered as it is where most of the execution time is spent. In the design II, the instruction slots which execute only writeback are not considered as an instruction for the calculation of \ac{IPC} as they are just the finalizing step of a previously executed operation.